/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author rodney2
 */
public class SymbolTable {
    private HashMap<Variable,VarState> states;
    private final Set<Value> used = new HashSet<Value>(3);

    public SymbolTable(int initialCapacity) {
        states = new HashMap<Variable,VarState> (initialCapacity);
    }

    public SymbolTable() {
        states = new HashMap<Variable,VarState> ();
    }

    public SymbolTable(SymbolTable t) {
        if (t == null || t.states == null)
            states = new HashMap<Variable, VarState>();
        else
            states = new HashMap<Variable,VarState> (t.states);
    }
    
    public void addUsed(Value val) {
        used.add(val);
    }
    
    public boolean isUsed(Value val) {
        return used.contains(val);
    }
    
    public void clearUsed() {
        used.clear();
    }

    public VarState get(Object key) {
        return states.get(key);
    }

    public VarState put(Variable key, VarState value) {
        return states.put(key, value);
    }

    public Set<Map.Entry<Variable, VarState>> entrySet() {
        return states.entrySet();
    }
    
    
    
}
