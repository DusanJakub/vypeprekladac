/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */
package cz.vutbr.fit.vype.chvalajakub.beans;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author rodney2
 */
public class RegisterMap {

    private final Map<Register, Value> keyToValueMap = new HashMap<Register, Value>();
    private final Map<Value, Register> valueToKeyMap = new HashMap<Value, Register>();

    public void put(Register key, Value value) {
        Value oldVal = keyToValueMap.put(key, value);
        if (oldVal != null) {
            valueToKeyMap.remove(oldVal);
        }
        
        if (value != null) {
            valueToKeyMap.put(value, key);
        }
    }

    public Value removeByKey(Register key) {
        Value removedValue = keyToValueMap.remove(key);
        valueToKeyMap.remove(removedValue);
        return removedValue;
    }

    public Register removeByValue(Value value) {
        Register removedKey = valueToKeyMap.remove(value);
        keyToValueMap.remove(removedKey);
        return removedKey;
    }

    public boolean containsKey(Register key) {
        return keyToValueMap.containsKey(key);
    }

    public boolean containsValue(Value value) {
        return valueToKeyMap.containsKey(value);
    }

    public Register getKey(Value value) {
        return valueToKeyMap.get(value);
    }

    public Value get(Register key) {
        return keyToValueMap.get(key);
    }
    
    public Collection<Register> keys() {
        return valueToKeyMap.values();
    }
    
    public Collection<Value> values() {
        return keyToValueMap.values();
    }
    
    public Set<Entry<Register,Value>> entrySet() {
        return keyToValueMap.entrySet();
    }
    
    public void clear() {
        keyToValueMap.clear();
        valueToKeyMap.clear();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Entry<Register,Value> e : entrySet()) {
            sb.append(e.getKey().getName()).append("=").append(e.getValue()).append(" ");
        }
        return sb.toString();
    }
    
    
}
