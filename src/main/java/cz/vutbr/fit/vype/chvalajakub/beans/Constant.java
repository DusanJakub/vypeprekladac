/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans;

/**
 *
 * @author rodney2
 */
public class Constant extends Value {
    private final String value;

    Constant(String value, DataType type) {
        this.value = value;
        this.type = type;
    }
    
    public String getValue() {
        if (type == DataType.CHAR)
            return String.valueOf((int)convertChar(value));
        else
            return value;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.value != null ? this.value.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Constant other = (Constant) obj;
        if ((this.value == null) ? (other.value != null) : !this.value.equals(other.value)) {
            return false;
        }
        return true;
    }
    
    
    
    public static char convertChar(String str) {
        char c = str.charAt(1);
        if (c == '\\') {
            c = str.charAt(2);
            switch (c) {
                case 'n': c = '\n'; break;
                case 'r': c = '\r'; break;
                case 't': c = '\t'; break;
                case '0': c = 0; break;
                default: break;
            }
        }
        return c;
    }
    
    
}
