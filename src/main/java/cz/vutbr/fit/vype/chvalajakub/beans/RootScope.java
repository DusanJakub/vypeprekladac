/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author rodney2
 */
public class RootScope extends Scope {
    private final Map<String,Constant> constants = new HashMap<String,Constant>();
        
    public RootScope() {
        super(null);
        
        
    }
        
    private int constCount = 0;
    public Constant registerConstant(String value, DataType type) {
        if (constants.containsKey(value)) {
            return constants.get(value);
        }
        else {
            Constant c = new Constant(value, type);
            String name = "$const$" + value.replaceAll("[^0-9a-zA-Z]+", ".");
            if (type == DataType.STRING && name.length() > 30) {
                name = name.substring(0, 30);
            }
            name += "$" + constCount++;
            c.setName(name);
            constants.put(value, c);
            return c;
        }
    }
    
    public Collection<Constant> getConstants() {
        return constants.values();
    }
}
