/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub;

import cz.vutbr.fit.vype.chvalajakub.beans.Function;
import cz.vutbr.fit.vype.chvalajakub.beans.SymbolTable;
import cz.vutbr.fit.vype.chvalajakub.beans.VarState;
import cz.vutbr.fit.vype.chvalajakub.beans.VarStateUpdate;
import cz.vutbr.fit.vype.chvalajakub.beans.Variable;
import cz.vutbr.fit.vype.chvalajakub.beans.instructions.Block;
import cz.vutbr.fit.vype.chvalajakub.beans.instructions.Instruction;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 *
 * @author rodney2
 */
public class Analyzer {
    public boolean analyzeBlock(Block block, SymbolTable endStateNew) {
        boolean changed = false;
                
        List<Instruction> instrs = new ArrayList<Instruction>(block.getImc());
        List<Variable> vars = block.getScope().getAll();
        List<VarStateUpdate> blockTable = new ArrayList<VarStateUpdate>(instrs.size());
        
        SymbolTable endState = block.getSymbolTableEnd();
        if (endState == null) {
            endState = new SymbolTable(block.getScope().totalSize());
            for (Variable var : vars) {
                endState.put(var, new VarState(false, null));
            }
            block.setSymbolTableEnd(endState);
            changed = true;
        }
        
        if (endStateNew != null) {
            for (Map.Entry<Variable,VarState> entry : endStateNew.entrySet()) {
                Variable var = entry.getKey();
                VarState oldState = endState.get(var);
                VarState newState = entry.getValue();
                
                if (oldState != null && !oldState.isLive() && newState.isLive()) {
                    endState.put(var, new VarState(true, null));
                    changed = true;
                }
            }
        }
        
        if (changed) {
            SymbolTable startState = new SymbolTable(endState);

            Collections.reverse(instrs);
            int i = instrs.size()-1;
            for (Instruction inst : instrs) {
                VarStateUpdate upd = new VarStateUpdate();
                if (inst.getTarget() != null) {
                    upd.put(inst.getTarget(), startState.put(inst.getTarget(), new VarState(false, null)));
                }
                if (inst.getOp1() != null && inst.getOp1() instanceof Variable) {
                    upd.put(inst.getOp1().asVariable(), startState.put(inst.getOp1().asVariable(), new VarState(true, i)));
                }
                if (inst.getOp2() != null && inst.getOp2() instanceof Variable) {
                    upd.put(inst.getOp2().asVariable(), startState.put(inst.getOp2().asVariable(), new VarState(true, i)));
                }
                blockTable.add(upd);
                i--;
            }
            Collections.reverse(blockTable);
            block.setSymbolTableStart(startState);
            block.setBlockTable(blockTable);
        }
        
        return changed;
    }
    
    private class BlockState {
        Block block;
        SymbolTable state;

        public BlockState(Block block, SymbolTable state) {
            this.block = block;
            this.state = state;
        }
        
        
    }
    
    public void analyzeFunction(Function func) {
        Queue<BlockState> queue = new ArrayDeque<BlockState>();
        queue.add(new BlockState(func.getLastBlock(), null));
        
        while (!queue.isEmpty()) {
            BlockState blockState = queue.poll();
            if (analyzeBlock(blockState.block, blockState.state)) {
                for (Block caller : blockState.block.getCallers()) {
                    queue.add(new BlockState(caller, new SymbolTable(blockState.block.getSymbolTableStart())));
                }
            }
        }
    }
}
