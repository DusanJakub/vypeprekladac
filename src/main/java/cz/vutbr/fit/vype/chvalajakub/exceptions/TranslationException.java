/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.exceptions;

import cz.vutbr.fit.vype.chvalajakub.beans.AppReturnCode;

/**
 *
 * @author jan.chvala
 */
public class TranslationException extends RuntimeException {
    
    public static final long serialVersionUID = 2;
    
    private final AppReturnCode retCode;

    public TranslationException(AppReturnCode retCode, String message, Throwable cause) {
        super(message, cause);
        this.retCode = retCode;
    }

    public AppReturnCode getRetCode() {
        return retCode;
    }
}
