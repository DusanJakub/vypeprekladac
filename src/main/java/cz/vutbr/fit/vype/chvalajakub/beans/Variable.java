/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */



package cz.vutbr.fit.vype.chvalajakub.beans;

/**
 *
 * @author rodney2
 */
public class Variable extends Value {
    private int offset;
    private Constant defaultValue;
    
    static public final char GENERATED_CHAR = '$';

    @Override
    public String toString() {
        return name;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
    
    public boolean isGenerated() {
        return name.charAt(0) == GENERATED_CHAR;
    }

    public Constant getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Constant defaultValue) {
        this.defaultValue = defaultValue;
    }
    
    
    
    
}
