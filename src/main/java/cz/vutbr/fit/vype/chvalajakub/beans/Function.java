/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */


package cz.vutbr.fit.vype.chvalajakub.beans;

import cz.vutbr.fit.vype.chvalajakub.beans.instructions.Block;
import java.util.ArrayList;
import java.util.List;

public class Function {
    private String name;
    private DataType[] paramTypes;
    private DataType returnType;
    private String signature;
    private Scope paramScope;
    private List<Block> blocks = new ArrayList<Block>();
    
    private int localFrameSize;
    private int callArgsFrameSize;
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(returnType == null ? "void" : returnType.name()).append(" ");
        sb.append(getSignature());
        if (paramScope != null) {
            sb.append(" (");
            sb.append(paramScope.toString());
            sb.append(")");
        }
        else {
            sb.append(" // not defined");
        }
        
        if (blocks != null) {
            for (Block block : blocks) {
                sb.append("\n");
                sb.append(block);
            }
        }
            
        
        return sb.toString();
    }
    
    public String getSignature() {
        if (signature == null)
            signature = getSignature(name, paramTypes);
        return signature;
    }
    
    public static String getSignature(String name, DataType[] types) {
        StringBuilder sb = new StringBuilder(name);
        sb.append(".");
        for (DataType type : types)
            sb.append(type.getInitial());
        return sb.toString();
    }
            
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataType[] getParamTypes() {
        return paramTypes;
    }

    public void setParamTypes(DataType[] paramTypes) {
        this.paramTypes = paramTypes;
    }

    public DataType getReturnType() {
        return returnType;
    }

    public void setReturnType(DataType returnType) {
        this.returnType = returnType;
    }

    public boolean isDefined() {
        return paramScope != null;
    }

    public Scope getParamScope() {
        return paramScope;
    }

    public void setParamScope(Scope paramScope) {
        this.paramScope = paramScope;
    }
    
    public void addBlock(Block block) {
        blocks.add(block);
    }

    public List<Block> getBlocks() {
        return blocks;
    }
    
    public Block getFirstBlock() {
        return blocks.get(0);
    }
    
    public Block getLastBlock() {
        return blocks.get(blocks.size()-1);
    }

    public int getLocalFrameSize() {
        return localFrameSize;
    }

    public int getCallArgsFrameSize() {
        return callArgsFrameSize;
    }

    public void enlargeLocalFrameSize(int localFrameSize) {
        if (localFrameSize > this.localFrameSize)
            this.localFrameSize = localFrameSize;
    }

    public void enlargeCallArgsFrameSize(int callArgsFrameSize) {
        if (callArgsFrameSize > this.callArgsFrameSize)
            this.callArgsFrameSize = callArgsFrameSize;
    }
    
    
}
