/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.exceptions;

import cz.vutbr.fit.vype.chvalajakub.beans.AppReturnCode;
import cz.vutbr.fit.vype.chvalajakub.beans.DataType;
import org.antlr.v4.runtime.Token;

/**
 *
 * @author rodney2
 */
public class SemanticError extends TranslationException {
    public static final long serialVersionUID = 1;

    public SemanticError(String message) {
        super(AppReturnCode.SEM_ERR, message, null);
    }

    public SemanticError(String message, Throwable cause) {
        super(AppReturnCode.SEM_ERR, message, cause);
    }
    
    public static SemanticError functionUndeclared(String name){
        return getSemanticError("Function '" + name + "' has not been declared.");
    }
    
    public static SemanticError functionUndefined(String name){
        return getSemanticError("Function '" + name + "' is not defined.");
    }
    
    public static SemanticError functionAllreadyDefined(String name){
        return getSemanticError("Function '" + name + "' has already been defined");
    }
    
    public static SemanticError functionAllreadyDefinedOrDeclared(String name){
        return getSemanticError("Function '" + name + "' has already been declared or defined");
    }

    public static SemanticError reservedFunctionName(String name) {
        return getSemanticError(name + " is a built-in function and cannot be overriden."); 
    }
    
    public static SemanticError voidFunctionInExpression(String name){
        return getSemanticError("Function '" + name + "' does not return value and thus cannot be used in expression.");
    }
    
    public static SemanticError definedFunctionReturnTypeMismatch(String name) {
        return getSemanticError("Function '" + name + "' has been declared differently than defined, mismatching return type.");
    }
    
    public static SemanticError returnInVoidRetFnc() {
        return getSemanticError("Cannot return a value from a void-returning function");
    }
    
    public static SemanticError castMismatchInReturnStmnt(DataType t1, DataType t2) {
        return getSemanticError("Cannot cast " + t1 + " to " + t2 + " in return statement");
    }
    
    public static SemanticError assignTypeMismatch(DataType t1, DataType t2) {
        return getSemanticError("Cannot assign " + t1 + " to " + t2);
    }
    
    public static SemanticError castMismatch(DataType t1, DataType t2) {
        return getSemanticError("Cannot cast " + t1 + " to " + t2);
    }
    
    public static SemanticError variableRedefined(String name) {
        return getSemanticError("Redefinition of variable '" + name + "'");
    }
    
    public static SemanticError variableUndefined(String name) {
        return getSemanticError("Variable '" + name + "' is not defined.");
    }
    
    public static SemanticError variableUndeclared(String name) {
        return getSemanticError("Variable '" + name + "' has not been declared");
    }
    
    public static SemanticError invalidOperandTypesError(String op, DataType type1, DataType type2){
        return getSemanticError("Invalid combination of operands for operator '" + op + "': " + type1.name() + ", " + type2.name());
    }
    
    public static SemanticError invalidTokenType(Token t){
        return getSemanticError("Invalid type: " + t.getText());
    }

    public static SemanticError invalidTypeInCondition(String stmt, DataType type) {    
        return getSemanticError("Only int type is allowed in condition of " +stmt+ ", " +type.name()+ " provided.");
    }

    public static SemanticError getSemanticError(String completeMsg){
        return new SemanticError(completeMsg);
    }
}
