#!/bin/bash

ERR=0
OK=0
for t in `ls`; do
	if [ -d $t ]; then
		if ./test.sh $t; then
			echo "$t: OK"
			OK=`expr $OK + 1`
		else
			echo "$t: Chyba"
			ERR=`expr $ERR + 1`
		fi
	fi
done

echo "-------------------"
echo "Celkem OK: $OK"
echo "Celkem chyb: $ERR"

