/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */


package cz.vutbr.fit.vype.chvalajakub.beans;

public enum DataType {
    INT('i'), CHAR('c'), SHORT('s'), STRING('S');
    
    private final char initial;
    DataType(char i) {
        this.initial = i;
    }
    
    public char getInitial() {
        return initial;
    }
}
