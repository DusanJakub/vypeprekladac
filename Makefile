#
# Compiler of VYPe13 language to MIPS assembly code
# -------------------------------------------------
# 
# Project for VYPe
# 2013
# 
# Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
# 

make:
	ant jar
	echo "#!/bin/sh" > vype
	echo 'java -jar `dirname "$$0"`/target/Vype13Compiler.jar $$@' >> vype
	chmod +x vype

clean:
	rm -f -R -d target/classes/*
	rm -f -R -d target/classes
	rm -f -R -d target/test-classes/*
	rm -f -R -d target/test-classes
	rm -f target/Vype13Compiler.jar
	rm -f -R -d libs/*
	rm -f -R -d libs
	rm -f vype

zip:	
	zip -r xjakub19.zip src target/generated-sources build.xml dokumentace.pdf Makefile maven-build.properties maven-build.xml pom.xml rozdeleni
