/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub;

import cz.vutbr.fit.vype.chvalajakub.beans.Constant;
import cz.vutbr.fit.vype.chvalajakub.beans.DataType;
import cz.vutbr.fit.vype.chvalajakub.beans.FrameMap;
import cz.vutbr.fit.vype.chvalajakub.beans.RegisterMap;
import cz.vutbr.fit.vype.chvalajakub.beans.Function;
import cz.vutbr.fit.vype.chvalajakub.beans.Register;
import cz.vutbr.fit.vype.chvalajakub.beans.RootScope;
import cz.vutbr.fit.vype.chvalajakub.beans.Scope;
import cz.vutbr.fit.vype.chvalajakub.beans.SymbolTable;
import cz.vutbr.fit.vype.chvalajakub.beans.Value;
import cz.vutbr.fit.vype.chvalajakub.beans.VarState;
import cz.vutbr.fit.vype.chvalajakub.beans.VarStateUpdate;
import cz.vutbr.fit.vype.chvalajakub.beans.Variable;
import cz.vutbr.fit.vype.chvalajakub.beans.instructions.Block;
import cz.vutbr.fit.vype.chvalajakub.beans.instructions.Instruction;
import cz.vutbr.fit.vype.chvalajakub.exceptions.SemanticError;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author rodney2
 */
public class AssemblerGenerator {
    
    private static final List<Register> REGISTERS = new ArrayList<Register>();
    private static final Register REG2 = new Register("$2");
    private static final Register REG3 = new Register("$3");
    
    private static final Register REG_RA = new Register("$ra");
    private static final Register REG_FP = new Register("$fp");
    private static final Register REG_SP = new Register("$sp");
    static {
        REGISTERS.add(new Register("$4"));
        REGISTERS.add(new Register("$5"));
        REGISTERS.add(new Register("$6"));
        REGISTERS.add(new Register("$7"));
        REGISTERS.add(new Register("$8"));
        REGISTERS.add(new Register("$9"));
        REGISTERS.add(new Register("$10"));
        REGISTERS.add(new Register("$11"));
        REGISTERS.add(new Register("$12"));
        REGISTERS.add(new Register("$13"));
        REGISTERS.add(new Register("$14"));
        REGISTERS.add(new Register("$15"));
        REGISTERS.add(new Register("$16"));
        REGISTERS.add(new Register("$17"));
        REGISTERS.add(new Register("$18"));
        REGISTERS.add(new Register("$19"));
        REGISTERS.add(new Register("$20"));
        REGISTERS.add(new Register("$21"));
        REGISTERS.add(new Register("$22"));
        REGISTERS.add(new Register("$23"));
        REGISTERS.add(new Register("$24"));
        REGISTERS.add(new Register("$25"));
        REGISTERS.add(new Register("$26"));
        REGISTERS.add(new Register("$27"));
    }
    
    private final Writer out;
    
    private SymbolTable symbolTable;
    private RegisterMap registerTable;
    private FrameMap memoryTable;
    private Scope currentScope;
    
    private final List<Variable> params = new ArrayList<Variable>();
    
    
    public AssemblerGenerator(Writer out) {
        this.out = out;
    }
    
    protected Register getRegister(Value val) throws IOException {
        Register reg = null;
               
        if (registerTable.containsValue(val)) {// if the variable is already in register, keep it there 
            return registerTable.getKey(val);
        }
        
        // if the var is in memory or nowhere, find
        // a) an empty register, if possible, or
        // b) a register containing a constant that will not be used in the upcoming instruction
        // c) a register containing a variable that will not be used in the upcoming instruction and will be used as the latest. 
        //    The original value must be stored in memory in this case
        
        int maxUsage = 0;
        Value selectedVal = null;
        for (Register r : REGISTERS) {
            Value regVal = registerTable.get(r);
            if (regVal == null) {
                return r;  // a)
            }
            else if (!symbolTable.isUsed(regVal)) {
                if (!regVal.isVariable()) {
                    return r; // b)
                }
                else {
                    VarState state = symbolTable.get(regVal);
                    if (!state.isLive()) {
                        return r;
                    }
                    else if (state.getNextUsage() == null || state.getNextUsage() > maxUsage) {
                        reg = r;
                        selectedVal = regVal;
                        if (state.getNextUsage() == null)
                            break;
                        else
                            maxUsage = state.getNextUsage();
                    }
                }
            }
        }
        
        if (selectedVal == null && REGISTERS.size() < 3)
            throw new NullPointerException("No free register - program needs at least 3 GPRs for its function");
        if (selectedVal.isVariable())
            storeToMemory(selectedVal.asVariable(), reg);
        registerTable.removeByKey(reg);
        return reg; // c)
    }
    
    public void generateCodeSection(Map<String,Function> funcs, RootScope globalScope) throws IOException {
        Function main = funcs.get("main.");
        if (main == null) {
            throw SemanticError.functionUndefined("main");
        }
        
        out.append(".text\n.org 0\n");
        writeInstruction("nop");
        writeInstruction("la", "$sp", "..stack");
        writeInstruction("la", "$fp", "..stack");
        writeInstruction("la", "$gp", "..heap");
        /*for (int i = REGISTERS.size()-1; i >= 0; i--) {
            writeInstruction("addi", REGISTERS.get(i).getName(), "$0", String.valueOf(i));
        }*/
        writeInstruction("jal", main.getSignature());
        writeInstruction("break");
        out.append("\n");
                
        out.append("$strcpy:\n");
        writeInstruction("lb", "$1", "0($2)");
        writeInstruction("sb", "$1", "0($3)");
        writeInstruction("addi", "$2", "$2", "1");
        writeInstruction("addi", "$3", "$3", "1");

        writeInstruction("bne", "$1", "$0", "$strcpy");
        writeInstruction("jr", "$ra");
        out.append("\n");
        
        
        out.append("$strcmp:\n");
        pushToStack("$4");
        out.append("$strcmp_next:\n");
        writeInstruction("lb", "$1", "0($2)");
        writeInstruction("lb", "$4", "0($3)");
        writeInstruction("addi", "$2", "$2", "1");
        writeInstruction("addi", "$3", "$3", "1");

        
        writeInstruction("bne", "$1", "$4", "$strcmp_end");
        writeInstruction("beq", "$1", "$0", "$strcmp_end");
        writeInstruction("j", "$strcmp_next");
        
        out.append("$strcmp_end:\n");
        writeInstruction("sub", "$2", "$1", "$4");
        popFromStack("$4");
        writeInstruction("jr", "$ra");
        
        out.append("\n");
        
        
        for (Function f : funcs.values()) {
            generateFunction(f);
        }
        
        for (Constant constant : globalScope.getConstants()) {
            if (constant.getType() == DataType.STRING) {
                out.append(constant.getName()).append(": .asciz ").append(constant.getValue()).append("\n");
            }
        }
        
        out.flush();
    }
    
    public void generateDataSection(RootScope globalScope) throws IOException {
        out.append("\n.data\n.org 0x1000\n");
        out.append("..stack:\n\n");
        out.append(".space 0x1000\n");
        out.append("..heap:\n");
        out.flush();
    }
    
    private void generateFunction(Function func) throws IOException {
        memoryTable = new FrameMap();
        memoryTable.setArgumentsScope(func.getParamScope());
        
        out.append(func.getSignature()).append(":\n");
        writeInstruction("add", "$fp", "$sp", "$0");
        writeInstruction("addi", "$sp", "$sp", String.valueOf(func.getLocalFrameSize() * 4));
        
        for (Block b : func.getBlocks()) {
            generateBlock(b);
        }
    }
    
    protected void generateBlock(Block block) throws IOException {
        symbolTable = new SymbolTable(block.getSymbolTableStart());
        registerTable = new RegisterMap();
        currentScope = block.getScope();

        out.append(block.getLabel()).append(":\n");
        
        for (int i = 0; i < block.getImc().size(); i++) {
            Instruction inst = block.getImc().get(i);
            VarStateUpdate upd = block.getBlockTable().get(i);
            
            generateInstruction(inst, upd);
            
            for (Entry<Variable,VarState> entry : upd.entrySet()) {
                symbolTable.put(entry.getKey(), entry.getValue());
            }
        }
        
        
    }
    
    protected void generateInstruction(Instruction inst, VarStateUpdate upd) throws IOException {
        Register regOp1 = null, 
                regOp2 = null, 
                regTarget = null;
        
        out.write("# ");
        out.write(inst.toString());
        out.write("\n");
        
        // Special instructions
        switch (inst.getCode()) {
            case PAR:
                if (!inst.getOp1().isVariable())
                    throw new RuntimeException("Invalid inner code: PAR can only take variables as parameters");
                params.add(inst.getOp1().asVariable());
                break;
                
            case CAL:
                Collection<Value> vals = new ArrayList<Value>(registerTable.values());
                for (Value v : vals) {
                    if (!v.isVariable()) {
                        registerTable.removeByValue(v);
                    }
                }
                
                /*for (Entry<Register,Value> entry : registerTable.entrySet()) {
                    if (entry.getValue().isVariable()) {
                        VarState state = symbolTable.get(entry.getValue().asVariable());
                        if (state.isLive()) {
                            storeToMemory(entry.getValue().asVariable(), entry.getKey());
                        }
                    }
                }
                registerTable.clear();*/
                
                List<Register> savedRegs = new ArrayList<Register>(registerTable.keys());
                savedRegs.add(REG_RA);
                savedRegs.add(REG_FP);
                pushRegsToStack(savedRegs);
                if (params.size() > 0) {
                    pushValsToStack(params);
                }
                writeInstruction("jal", inst.getFunc().getSignature());
                
                
                // remove parameters from stack
                if (params.size() > 0) {
                    writeInstruction("addi", "$sp", "$sp", "-"+String.valueOf(params.size()*4));
                    params.clear();
                }
                popFromStack(savedRegs);
                
                if (inst.getTarget() != null) {
                    regTarget = getRegister(inst.getTarget());
                    registerTable.put(regTarget, inst.getTarget());
                    writeInstruction("add", regTarget.getName(), "$2", "$0");
                }
                break;
                
            default:
                if (inst.getOp1() != null) {
                    regOp1 = getRegister(inst.getOp1());
                    symbolTable.addUsed(inst.getOp1());
                    loadToRegister(inst.getOp1(), regOp1);
                }
                if (inst.getOp2() != null) {
                    regOp2 = getRegister(inst.getOp2());
                    symbolTable.addUsed(inst.getOp2());
                    loadToRegister(inst.getOp2(), regOp2);
                }
                if (inst.getTarget() != null) {
                    regTarget = getRegister(inst.getTarget());
                    registerTable.put(regTarget, inst.getTarget());
                }

                generateInstruction0(inst, regTarget, regOp1, regOp2);

                symbolTable.clearUsed();
                break;
        }
        
        
        dumpRegisterTable();
        out.write("\n");
    }
    
    private void loadToRegister(Value val, Register reg) throws IOException {
        Value oldVal = registerTable.get(reg);
        if (oldVal != val) {
            if (val.isVariable()) {
                    writeInstruction("lw", reg.getName(), memoryTable.get(val.asVariable(), currentScope));
            }
            else { // constant
                loadValueToRegister(val.asConstant(), reg);
            }
            registerTable.put(reg, val);
            dumpRegisterTable();
        }
    
    }
    
    private void loadDefaultToRegister(Variable var, Register reg) throws IOException {
        Value oldVal = registerTable.get(reg);
        if (oldVal != var) {
            loadValueToRegister(var.getDefaultValue(), reg);
            registerTable.put(reg, var);
            dumpRegisterTable();
        }
    
    }
    
    private void loadValueToRegister(Constant value, Register reg) throws IOException {
        switch (value.getType()) {
            case CHAR:
                writeInstruction("addi", reg.getName(), "$0", value.getValue());
                break;
                
            case INT:
                int ivalue = Integer.valueOf(value.getValue());
                //if (ivalue <= 0xffff) {
                    writeInstruction("li", reg.getName(), String.valueOf(ivalue));
                /*}
                else {
                    writeInstruction("lui", reg.getName(), String.valueOf(ivalue & 0xffff));
                    writeInstruction("ori", reg.getName(), reg.getName(), String.valueOf(ivalue >> 16));
                }*/
                break;
                
            case STRING:
                writeInstruction("addi", reg.getName(), "$0", value.getName());
                break;
        }
    }
    
    // TODO: dodelat
    private void storeToMemory(Variable var, Register reg) throws IOException {
        writeInstruction("sw", reg.getName(), memoryTable.get(var, currentScope));
        memoryTable.init(var, currentScope);
    }
    
    private void pushToStack(String what) throws IOException {
        writeInstruction("sw", what, "0($sp)");
        writeInstruction("addi", "$sp", "$sp", "4");
    }
    
    private void popFromStack(String what) throws IOException {
        writeInstruction("addi", "$sp", "$sp", "-4");
        writeInstruction("lw", what, "0($sp)");
    }
    
    
    
    private void pushRegsToStack(List<Register> list) throws IOException {
        int i = 0;
        for (Register r : list) {
            writeInstruction("sw", r.getName(), (i*4)+"($sp)");
            i++;
        }
        writeInstruction("addi", "$sp", "$sp", ""+(list.size()*4));
    }
    
    private void pushValsToStack(List<? extends Value> list) throws IOException {
        int i = 0;
        for (Value v : list) {
            Register r = getRegister(v);
            loadToRegister(v, r);
            writeInstruction("sw", r.getName(), (i*4)+"($sp)");
            i++;
        }
        writeInstruction("addi", "$sp", "$sp", ""+(list.size()*4));
    }
    
    private void popFromStack(List<Register> list) throws IOException {
        writeInstruction("addi", "$sp", "$sp", "-"+(list.size()*4));
        int i = 0;
        for (Register r : list) {
            writeInstruction("lw", r.getName(), (i*4)+"($sp)");
            i++;
        }
    }
    
    private void generateInstruction0(Instruction inst, Register target, Register op1, Register op2) throws IOException {
        
        switch (inst.getCode()) {
            case NEG: 
                writeInstruction("sub", target.getName(), "$0", op1.getName()); 
                break;
                
            case MOV:
                writeInstruction("movz", target.getName(), op1.getName(), "$0");
                break;
                
            case MOVS: 
                pushToStack(REG_RA.getName());
                writeInstruction("add", "$2", op1.getName(), "$0");
                writeInstruction("add", "$3", "$gp", "$0");
                writeInstruction("add", target.getName(), "$gp", "$0");
                
                writeInstruction("jal", "$strcpy");
                
                writeInstruction("addi", "$gp", "$3", "-1");
                writeInstruction("srl", "$gp", "$gp", "2"); // we must round the next address up to multiply of 4
                writeInstruction("addi", "$gp", "$gp", "1");
                writeInstruction("sll", "$gp", "$gp", "2");
                popFromStack(REG_RA.getName());
                break;
                
            case MOVCS:
                writeInstruction("add", "$3", op1.getName(), op2.getName());
                writeInstruction("lb", target.getName(), "0($3)");
                break;
                
            case MOVSC:
                writeInstruction("sb", op2.getName(), "0(" + op1.getName() + ")");
                break;
                
            case STRCAT:
                pushToStack(REG_RA.getName());
                writeInstruction("add", "$2", op1.getName(), "$0");
                writeInstruction("add", "$3", "$gp", "$0");
                writeInstruction("add", target.getName(), "$gp", "$0");
                
                writeInstruction("jal", "$strcpy");
                
                
                writeInstruction("add", "$2", op2.getName(), "$0");
                writeInstruction("addi", "$3", "$3", "-1");
                
                writeInstruction("jal", "$strcpy");
                
                writeInstruction("addi", "$gp", "$3", "-1");
                writeInstruction("srl", "$gp", "$gp", "2"); // we must round the next address up to multiply of 4
                writeInstruction("addi", "$gp", "$gp", "1");
                writeInstruction("sll", "$gp", "$gp", "2");
                popFromStack(REG_RA.getName());
                break;
              
            /***************** Binary math operators ***********************/
            case ADD:
                writeInstruction("add", target.getName(), op1.getName(), op2.getName());
                break;
                
            case SUB:
                writeInstruction("sub", target.getName(), op1.getName(), op2.getName());
                break;
            
            case MUL:
                writeInstruction("mul", target.getName(), op1.getName(), op2.getName());
                break;
                
            case DIV:
                writeInstruction("div", op1.getName(), op2.getName());
                writeInstruction("mflo", target.getName());
                break;
                
            case REM:
                writeInstruction("div", op1.getName(), op2.getName());
                writeInstruction("mfhi", target.getName());
                break;
                
            /******************* Int comparison ***********************/
            case LTI:
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("sub", "$1", op1.getName(), op2.getName());
                writeInstruction("bgez", "$1", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case LTEI:
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("sub", "$1", op1.getName(), op2.getName());
                writeInstruction("bgtz", "$1", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case GTI:
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("sub", "$1", op1.getName(), op2.getName());
                writeInstruction("blez", "$1", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case GTEI:
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("sub", "$1", op1.getName(), op2.getName());
                writeInstruction("bltz", "$1", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case EQI:
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("bne", op1.getName(), op2.getName(), ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case NEQI:
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("beq", op1.getName(), op2.getName(), ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
                
            /******************* String comparison ***********************/    
            case LTS:
                pushToStack(REG_RA.getName());
                writeInstruction("add", "$2", op1.getName(), "$0");
                writeInstruction("add", "$3", op2.getName(), "$0");
                writeInstruction("jal", "$strcmp");
                popFromStack(REG_RA.getName());
                                
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("bgez", "$2", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case LTES:
                pushToStack(REG_RA.getName());
                writeInstruction("add", "$2", op1.getName(), "$0");
                writeInstruction("add", "$3", op2.getName(), "$0");
                writeInstruction("jal", "$strcmp");
                popFromStack(REG_RA.getName());
                
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("bgtz", "$2", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case GTS:
                pushToStack(REG_RA.getName());
                writeInstruction("add", "$2", op1.getName(), "$0");
                writeInstruction("add", "$3", op2.getName(), "$0");
                writeInstruction("jal", "$strcmp");
                popFromStack(REG_RA.getName());
                
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("blez", "$2", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case GTES:
                pushToStack(REG_RA.getName());
                writeInstruction("add", "$2", op1.getName(), "$0");
                writeInstruction("add", "$3", op2.getName(), "$0");
                writeInstruction("jal", "$strcmp");
                popFromStack(REG_RA.getName());
                
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("bltz", "$2", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case EQS:
                pushToStack(REG_RA.getName());
                writeInstruction("add", "$2", op1.getName(), "$0");
                writeInstruction("add", "$3", op2.getName(), "$0");
                writeInstruction("jal", "$strcmp");
                popFromStack(REG_RA.getName());
                
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("bne", "$2", "$0", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case NEQS:
                pushToStack(REG_RA.getName());
                writeInstruction("add", "$2", op1.getName(), "$0");
                writeInstruction("add", "$3", op2.getName(), "$0");
                writeInstruction("jal", "$strcmp");
                popFromStack(REG_RA.getName());
                
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("beq", "$2", "$0", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
                
            /******************* Logical operators ***********************/
            case NOT:
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("bne", op1.getName(), "$0", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case MOVB:
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("beq", op1.getName(), "$0", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1");
                break;
                
            case AND:
                writeInstruction("ori", target.getName(), "$0", "1"); // 
                writeInstruction("bne", op1.getName(), "$0", ".+8");
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("bne", op2.getName(), "$0", ".+8");
                writeInstruction("movz", target.getName(), "$0", "$0");
                break;
                
            case OR:
                writeInstruction("movz", target.getName(), "$0", "$0");
                writeInstruction("beq", op1.getName(), "$0", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1"); //
                writeInstruction("beq", op2.getName(), "$0", ".+8");
                writeInstruction("ori", target.getName(), "$0", "1"); //
                break;
                
            case C2I:
                // do nothing as both chars and integers are physically stored in 32b registers
                writeInstruction("movz", target.getName(), op1.getName(), "$0");
                break;
                
            case I2C:
                writeInstruction("andi", target.getName(), op1.getName(), "0xff");
                break;
                
            case C2S:
                writeInstruction("sw", "$0", "0($gp)");
                writeInstruction("sb", op1.getName(), "0($gp)");
                writeInstruction("add", target.getName(), "$gp", "$0");
                writeInstruction("addi", "$gp", "$gp", "4");
                break;
                
            case PRINTI:
                writeInstruction("print_int", op1.getName());
                break;
                
            case PRINTC:
                writeInstruction("print_char", op1.getName());
                break;
                
            case PRINTS:
                writeInstruction("print_string", op1.getName());
                break;
                
            case READ_INT:
                writeInstruction("read_int", target.getName());
                break;
                
            case READ_CHAR:
                writeInstruction("read_char", target.getName());
                break;
                
            case READ_STR:
                writeInstruction("add", target.getName(), "$gp", "$0");
                writeInstruction("read_string", "$gp", "$2");
                writeInstruction("add", "$gp", "$gp", "$2");
                writeInstruction("sb", "$0", "0($gp)");
                
                
                writeInstruction("srl", "$gp", "$gp", "2"); // we must round the next address up to multiply of 4
                writeInstruction("addi", "$gp", "$gp", "1");
                writeInstruction("sll", "$gp", "$gp", "2");
                break;
                
                
            case JMP:
                writeInstruction("j", inst.getJump().getLabel());
                break;
                
            case JEZ:
                writeInstruction("beq", op1.getName(), "$0", inst.getJump().getLabel());
                break;
                
            case JNZ:
                writeInstruction("bne", op1.getName(), "$0", inst.getJump().getLabel());
                break;
               
            case RET:
                if (op1 != null)
                    writeInstruction("add", "$2", op1.getName(), "$0");
                else
                    writeInstruction("add", "$2", "$0", "$0");
                writeInstruction("add", "$sp", "$fp", "$0");
                //popFromStack("$fp");
                    
                writeInstruction("jr", "$ra");
                break;
                
                
            case INIT_FRAME:
                for (Variable v : inst.getJump().getScope().getLocal()) {
                    if (symbolTable.get(v).isLive()) {
                        Register reg = getRegister(v.getDefaultValue());
                        loadDefaultToRegister(v, reg);
                    }
                }
                break;
                
            case STORE:
                for (Entry<Register,Value> entry : registerTable.entrySet()) {
                    if (entry.getValue().isVariable()) {
                        VarState state = symbolTable.get(entry.getValue().asVariable());
                        if (state.isLive()) {
                            storeToMemory(entry.getValue().asVariable(), entry.getKey());
                        }
                    }
                }
                break;
                
                
                
        }
    }
    
    private void writeInstruction(String name, String... op) throws IOException {
        out.write(name);
        out.write(" ");

        for (int i = 0; i < op.length; i++) {
            if (i > 0) out.write(", ");
            out.write(op[i]);
        }
        out.write("\n");
        out.flush();
    }
    
    private int labelCnt = 0;
    private String createLabel(String label) {
        labelCnt++;
        return "$" + label + "$" + labelCnt;
    }
    
    private void dumpRegisterTable() throws IOException {
        out.write("# ");
        out.write(registerTable.toString());
        out.write("\n");
    }
}
