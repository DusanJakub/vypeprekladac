/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans.instructions;

import cz.vutbr.fit.vype.chvalajakub.beans.Scope;
import cz.vutbr.fit.vype.chvalajakub.beans.SymbolTable;
import cz.vutbr.fit.vype.chvalajakub.beans.VarStateUpdate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rodney2
 */
public class Block {
    private final Scope scope;
    private final List<Instruction> imc;
    private final String label;
    private final List<Block> callers;
    private List<VarStateUpdate> blockTable;
    private SymbolTable symbolTableStart;
    private SymbolTable symbolTableEnd;

    private static int count = 0;
    
    public Block(Scope scope, String label) {
        this.scope = scope;
        this.label = label + count++;
        this.imc = new ArrayList<Instruction>();
        this.callers = new ArrayList<Block>();
    }

    public Scope getScope() {
        return scope;
    }

    public List<Instruction> getImc() {
        return imc;
    }

    public String getLabel() {
        return label;
    }
    
    public void addInstruction(Instruction instruction) {
        imc.add(instruction);
    }
    
    public void addCaller(Block caller) {
        callers.add(caller);
    }

    @Override
    public String toString() {
        StringBuilder sb = new  StringBuilder();
        sb.append(".").append(label).append(" // called from: ");
        for (Block caller : callers) {
            sb.append(caller.getLabel()).append(" ");
        }
        sb.append("\n  // Vars:\n");
        sb.append(scope);
        sb.append("\n  // Code:\n");
        
        for (int i = 0; i < imc.size(); i++) {
            sb.append("  ").append(i).append(": ").append(imc.get(i));
            if (blockTable != null)
                sb.append("  //  ").append(blockTable.get(i));
            sb.append("\n");
        }
        return sb.toString();
    }

//    public VarState[][] getBlockTable() {
//        if (blockTable == null)
//            blockTable = new VarState[imc.size()][scope.totalSize()];
//        return blockTable;
//    }
    public List<VarStateUpdate> getBlockTable() {
        return blockTable;
    }

    public void setBlockTable(List<VarStateUpdate> blockTable) {
        this.blockTable = blockTable;
    }

    public SymbolTable getSymbolTableStart() {
        return symbolTableStart;
    }

    public void setSymbolTableStart(SymbolTable symbolTableStart) {
        this.symbolTableStart = symbolTableStart;
    }

    public SymbolTable getSymbolTableEnd() {
        return symbolTableEnd;
    }

    public void setSymbolTableEnd(SymbolTable symbolTableEnd) {
        this.symbolTableEnd = symbolTableEnd;
    }

    public List<Block> getCallers() {
        return callers;
    }
    
    
}
