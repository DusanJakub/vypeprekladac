/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub;

import cz.vutbr.fit.vype.chvalajakub.VypeParser.ProgContext;
import cz.vutbr.fit.vype.chvalajakub.beans.AppReturnCode;
import cz.vutbr.fit.vype.chvalajakub.beans.Constant;
import cz.vutbr.fit.vype.chvalajakub.beans.Function;
import cz.vutbr.fit.vype.chvalajakub.exceptions.InternalErrors;
import cz.vutbr.fit.vype.chvalajakub.exceptions.LexicalException;
import cz.vutbr.fit.vype.chvalajakub.exceptions.SyntaxException;
import cz.vutbr.fit.vype.chvalajakub.exceptions.TranslationException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.logging.Logger;
import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;

/**
 * VYPe13
 */
public class App 
{
    private static final Logger LOGGER = Logger.getLogger(App.class.getName());
    private static final String DEFAULT_OUTPUT_FILENAME = "out.asm";
    
    
    public static void main( String[] args )
    {
        try {
            mainFunction(args);
        } catch (TranslationException e) {
            exitProgram(e.getMessage(), e.getRetCode());
        } catch (Exception e){
            exitProgram("Unknown error during compilation.", AppReturnCode.INTERN_ERR);
        }
        
        exitProgram(AppReturnCode.NO_ERR);
    }
    
    /**
     * this function is basicaly body of main function which throws TranslationException
     * in case of error. This separation is needded in tests.
     * 
     * @param args
     * @throws TranslationException
     */
    public static void mainFunction( String[] args ) throws TranslationException
    {
        ANTLRFileStream inputStream = null;
        OutputStream outputStream = null;
        String inputFilename, outputFilename;
     
        if (args == null || args.length < 1) {
            throw InternalErrors.printHelp();
        }
        
        inputFilename = args[0];
        outputFilename = args.length >= 2 ? args[1] : DEFAULT_OUTPUT_FILENAME;
        
        try {
            inputStream = new ANTLRFileStream(inputFilename);
        } catch (IOException ex) {
            throw InternalErrors.unableOpenInputFile(inputFilename);
        }
        
        try {
            outputStream = new FileOutputStream(outputFilename);
        } catch (FileNotFoundException ex) {
            throw InternalErrors.unableOpenOutputFile(outputFilename);
        }
                
        
        /* overriding nextToken function in lexer to stop lexical analysis on unknown token */
        VypeLexer lexer = new VypeLexer(inputStream){
            @Override
            public Token nextToken() {
                Token t = super.nextToken();
                if(t.getType() == VypeLexer.UNKNOWN){
                    throw LexicalException.lexExp(t.getText(), t.getLine(), t.getCharPositionInLine());
                }
                return t;
            }
            
        };

        /* overriding match function in parser to stop syntax analysis on when token has diferent type than we expect */
        VypeParser parser = new VypeParser(new CommonTokenStream(lexer)){
            @Override
            public Token match(int ttype) throws RecognitionException {
                Token t = getCurrentToken();
                if ( t.getType()==ttype ) {
                    _errHandler.reportMatch(this);
                    consume();
                    return t;
                }

                throw SyntaxException.synExp(t, ttype);
            }

        };
        
        parser.addErrorListener(new ANTLRErrorListener<Token>() {
            @Override
            public <T extends Token> void syntaxError(Recognizer<T, ?> recognizer, T offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                throw SyntaxException.synExp(msg);
            }
        });
        
        ProgContext ctx = parser.prog();
        
        InterCodeGenerator imcGen = new InterCodeGenerator();
        Analyzer optim = new Analyzer();
        OutputStreamWriter osw = new OutputStreamWriter(outputStream);
        AssemblerGenerator asmGen = new AssemblerGenerator(osw);
        
        try {
            imcGen.visit(ctx);
            imcGen.checkDeclaredFunctionsAreDefined(optim);
            
            //printIntermediateCodeDebugInformation(imcGen);
        
            asmGen.generateCodeSection(imcGen.getFunctions(), imcGen.getRootScope());
            asmGen.generateDataSection(imcGen.getRootScope());
        } catch (IOException ex) {
            throw InternalErrors.unableToWriteToOutputFile(outputFilename);
        }
    }
    
    private static void exitProgram(AppReturnCode code)
    {
        exitProgram(null, code);
    }
    
    private static void exitProgram(String msg, AppReturnCode code)
    {
        if(msg != null){
            System.err.println(msg);
        }
        
        System.exit(code.getReturnCode());
    }

    private static void printIntermediateCodeDebugInformation(InterCodeGenerator imcGen) {
        for (Function f : imcGen.getFunctions().values()) {
            LOGGER.info(f.toString());
        }

        for (Constant c : imcGen.getConstants()) {
            LOGGER.info(String.format(".%s %s", c.getName(), c.getValue()));
        }
    }
    
    public static String getTokenNameFromType(int ttype){
        try {
            return VypeParser.tokenNames[ttype];
        } catch (Exception e) {
        }
        return String.valueOf(ttype);
    }
}
