/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */
package cz.vutbr.fit.vype.chvalajakub;

import org.junit.Test;

/**
 *
 * @author admin
 */
public class OneTest extends AbstractTest {
    
    @Test
    public void testOnlyThisOne() {
        processTest("0_while3");
    }

}
