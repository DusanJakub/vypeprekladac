/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */
package cz.vutbr.fit.vype.chvalajakub.exceptions;

import cz.vutbr.fit.vype.chvalajakub.beans.AppReturnCode;

/**
 *
 * @author admin
 */
public class LexicalException extends TranslationException {
    public static final long serialVersionUID = 1;

    public LexicalException(String message, Throwable cause) {
        super(AppReturnCode.LEX_ERR, message, cause);
    }

    public LexicalException(String message) {
        super(AppReturnCode.LEX_ERR, message, null);
    }
    
    
    public static LexicalException lexExp(String token, int line, int charPos) {
        return getLexicalException(String.format("unknown token[%s] on line[%d] at position[%d].", token, line, charPos));
    }
    
    public static LexicalException lexExp() {
        return getLexicalException("unknown lexical exception");
    }
    
    
    public static LexicalException getLexicalException(String completeMsg){
        return new LexicalException(completeMsg);
    }

}
