grammar Vype;


prog: (functionDecl | functionDef)*;

// lexer:
INT_TYPE: 'int';
SHORT_TYPE: 'short';
CHAR_TYPE: 'char';
STRING_TYPE: 'string';
VOID_TYPE: 'void';

ID 		: [a-zA-Z_] [a-zA-Z_0-9]*;
INT_CONSTANT	: [0-9]+;

fragment CHAR1	: '\\n' | '\\t' | '\\\\' | '\\\'' | '\\"' | ~[\u0000-\u001F\u0022\u0027];
CHAR_CONSTANT	: '\'' (CHAR1 | '"' | '\\0') '\'';
STRING_CONSTANT	: '"' (CHAR1 | '\'')* '"';

PLUS            : '+';
MINUS           : '-';
TIMES           : '*';
DIV             : '/';
REM             : '%';
LT              : '<';
GT              : '>';
LTE             : '<=';
GTE             : '>=';
EQ              : '==';
NEQ             : '!=';
AND             : '&&';
OR              : '||';
NOT             : '!';

LINE_COMMENT 	: '//' ~[\n\r]* [\n\r] 
	-> skip;
BLOCK_COMMENT 	: '/*' (~'*'|'*' ~'/')* '*/' 
	-> skip;
BLANK 		: [ \t\n\r]+
	-> skip;

UNKNOWN         : . ;


// parser:
dataType 	: INT_TYPE | CHAR_TYPE | STRING_TYPE;
type		: dataType | VOID_TYPE;

functionDecl	: ret=type name=ID '(' (VOID_TYPE | types+=dataType (',' types+=dataType)* ) ')' ';'; 
functionDef	: ret=type name=ID '(' (VOID_TYPE | params+=varDecl1 (',' params+=varDecl1)* ) ')' block;

varDecl1        : dataType ID;
initId          : ID ('=' exp)? ;
varDecl		: dataType initId (',' initId)*;

block		: '{' statement* '}';

assignStmt	: ID '=' exp;
ifElseStmt      : 'if' '(' exp ')' ifBlock=block 'else' elseBlock=block;
ifOnlyStmt      : 'if' '(' exp ')' ifBlock=block;
whileStmt	: 'while' '(' exp ')' block;
returnStmt	: 'return' exp?;
funcStmt        : ID '(' ( params+=exp (',' params+=exp )* )? ')';

statement	: assignStmt ';' 
                | ifElseStmt 
                | ifOnlyStmt
                | whileStmt 
                | returnStmt ';' 
                | funcStmt ';' 
                | varDecl ';'
                ;

constant        : INT_CONSTANT
		| CHAR_CONSTANT
		| STRING_CONSTANT
                ;

exp
		: op=(MINUS | PLUS) exp                 #unaryExpr
		| '(' exp ')'				#parensExpr
		| '(' dataType ')' exp			#castExpr
		| ID '(' ( params+=exp (',' params+=exp )* )? ')' 	#funcExpr
                | op=NOT exp                               #notExpr
		| op1=exp op=(TIMES | DIV | REM) op2=exp		#multExpr
		| op1=exp op=(PLUS | MINUS) op2=exp			#addExpr
		| op1=exp op=(LT | GT | LTE | GTE) op2=exp 	#compExpr
		| op1=exp op=(NEQ | EQ) op2=exp			#equalsExpr
		| op1=exp op=AND op2=exp				#andExpr
		| op1=exp op=OR op2=exp				#orExpr
		| ID					#idExpr
		| constant                              #constExpr
		;

