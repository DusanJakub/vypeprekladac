#!/bin/bash
# Ukazka zpracovani jazyku symbolickych adres pro MIPS32 vcetne simulace
# na Linuxovem OS (32-bitovem)
# Pouziti:
#   ./make_and_run.sh jmeno_asm_bez_pripony

EXP=${1:0:1}
FILE=test/$1/$1
cd ..
./vype $FILE.vype $FILE.asm 

RET=$?

if [ $EXP != $RET ]; then
	echo "$1: Spatny navratovy kod. Mel byt $EXP, byl $RET"
	exit 1;
fi

if [ $RET != 0 ]; then
	exit 0;
fi

# preklad asembleru do objektoveho souboru (prida 0 za koncovku obj)
wine asm/assembler2.exe -i $FILE.asm -o $FILE.obj

# spojeni objektoveho souboru do binarniho souboru
wine asm/linker.exe $FILE.obj -o $FILE.xexe

# simulace binarnho souboru s programem nad zadanym vstupem mips.input
if [ -f $FILE.in ]; then
	wine asm/intersim2.exe -V 0 -i $FILE.xexe -x $FILE.xml -n mips < $FILE.in > $FILE.stdout 2> $FILE.stderr
else
	wine asm/intersim2.exe -V 0 -i $FILE.xexe -x $FILE.xml -n mips > $FILE.stdout 2> $FILE.stderr
fi

STR="\\-\\-\\- Simulation ended"
OFFSET=`grep -bo "$STR" $FILE.stdout | sed s/:.*//`
head -c $OFFSET $FILE.stdout > $FILE.stdout2

if diff --ignore-all-space $FILE.out $FILE.stdout2; then
	exit 0
else
	exit 1
fi
