/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

/**
 *
 * @author rodney2
 */
public class FrameMap {
    private Map<Scope,ScopeMap> knownScopes = new WeakHashMap<Scope, ScopeMap>();
    
    /**
     * Find the offset of the variable in the frame. If the var has not been initialized
     * return null and mark it initialized for future calls.
     * 
     * @param var The variable, defined in the supplied scope or its subscopes
     * @param scope The cope
     * @return The position if the variable is part of the stack frame and has been initialized,
     * null if the variable is part of the stack frame, but has not been accesed yet
     * @throws RuntimeException if the var is not part of the stack frame
     */
    public String get(Variable var, Scope scope) {
        ScopeMap map = getScopeMapForVar(var, scope);
        return map.get(var);
    }
    
    public boolean isInit(Variable var, Scope scope) {
        ScopeMap map = getScopeMapForVar(var, scope);
        return map.isInit(var);
    }
    
    public void init(Variable var, Scope scope) {
        ScopeMap map = getScopeMapForVar(var, scope);
        map.init(var);
    }
    
    private ScopeMap getScopeMapForVar(Variable var, Scope scope) {
        while (scope != null) {
            ScopeMap map = getScopeMap(scope);
            if (map.contains(var))
                return map;
            scope = scope.getParent();
        }
        throw new RuntimeException("Supplied variable " + var.toString() + " is not part of the scope");
    }
    
    public void setArgumentsScope(Scope scope) {
        ScopeMap scopeMap = new ScopeMap("$fp", scope, -scope.size(), 0);
        knownScopes.put(scope, scopeMap);
        for (Variable v : scope.getLocal()) {
            scopeMap.init(v);
        }
    }
    
    protected ScopeMap getScopeMap(Scope scope) {
        ScopeMap map = knownScopes.get(scope);
        if (map == null) {
            ScopeMap parent = getScopeMap(scope.getParent());
            map = new ScopeMap("$fp", scope, parent.getNextOffset());
            knownScopes.put(scope, map);
        }
        return map;
    }
    
    /*public int getLocalFrameSize(Scope scope) {
        return getScopeMap(scope).getNextOffset();
    }*/
    
    protected class ScopeMap {
        private final Map<Variable,Integer> offsets;
        private final Set<Variable> initialized;
        private final String base;
        private final int scopeOffset;
        private final int nextOffset;

        public ScopeMap(String base, Scope scope, int scopeOffset, int nextOffset) {
            this.base = base;
            this.scopeOffset = scopeOffset;
            this.nextOffset = nextOffset;
            this.offsets = new HashMap<Variable, Integer>(scope.size());
            this.initialized = new HashSet<Variable>(scope.size());
            
            int i = 0;
            for (Variable var : scope.getLocal()) {
                offsets.put(var, i);
                i++;
            }
        }
        
        public ScopeMap(String base, Scope scope, int scopeOffset) {
            this(base, scope, scopeOffset, scopeOffset + scope.size());
        }
        
        public String get(Variable var) {
            Integer offset = offsets.get(var);
            if (offset == null)
                return null;
            
            offset += scopeOffset;
            return (offset * 4) + "(" + base + ")";
        }
        
        public boolean contains(Variable var) {
            return offsets.containsKey(var);
        }
        
        public boolean isInit(Variable var) {
            return initialized.contains(var);
        }
        
        public void init(Variable var) {
            if (offsets.containsKey(var))
                initialized.add(var);
        }

        public int getNextOffset() {
            return nextOffset;
        }
    }
}
