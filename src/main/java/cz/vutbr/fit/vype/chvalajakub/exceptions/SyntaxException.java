/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */
package cz.vutbr.fit.vype.chvalajakub.exceptions;

import cz.vutbr.fit.vype.chvalajakub.App;
import cz.vutbr.fit.vype.chvalajakub.beans.AppReturnCode;
import org.antlr.v4.runtime.Token;

/**
 *
 * @author admin
 */
public class SyntaxException extends TranslationException {
    public static final long serialVersionUID = 1;

    public SyntaxException(String message, Throwable cause) {
        super(AppReturnCode.SYN_ERR, message, cause);
    }

    public SyntaxException(String message) {
        super(AppReturnCode.SYN_ERR, message, null);
    }
    
    
    public static SyntaxException synExp() {
        return synExp("");
    }
    
    public static SyntaxException synExp(String msg) {
        return getSyntaxException("syntax error" + (msg == null ? "" : ": " + msg));
    }
    
    public static SyntaxException synExp(Token actual, int expectedType) {
        return synExp(String.format(
                "syntax error on line[%s] at position[%s]. Expectiong token\"%s\" but get token\"%s\"."
                , actual.getLine(), actual.getCharPositionInLine()
                , App.getTokenNameFromType(expectedType)
                , App.getTokenNameFromType(actual.getType())
        ));
    }
    
    
    public static SyntaxException getSyntaxException(String completeMsg){
        return new SyntaxException(completeMsg);
    }

}
