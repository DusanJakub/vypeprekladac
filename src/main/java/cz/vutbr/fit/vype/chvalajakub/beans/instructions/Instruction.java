/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans.instructions;

import cz.vutbr.fit.vype.chvalajakub.beans.Function;
import cz.vutbr.fit.vype.chvalajakub.beans.Value;
import cz.vutbr.fit.vype.chvalajakub.beans.Variable;

/**
 *
 * @author rodney2
 */
public class Instruction {
    private final OperationCode code;
    private final Variable target;
    private final Block jump;
    private final Function func;
    private final Value op1;
    private final Value op2;

    public Instruction(OperationCode code, Variable target, Value op1, Value op2) {
        this.code = code;
        this.target = target;
        this.op1 = op1;
        this.op2 = op2;
        this.jump = null;
        this.func = null;
    }
    
    public Instruction(OperationCode code, Block jump, Value op1) {
        this.code = code;
        this.target = null;
        this.op1 = op1;
        this.op2 = null;
        this.jump = jump;
        this.func = null;
    }
    
    public Instruction(OperationCode code, Variable target, Function func) {
        this.code = code;
        this.target = target;
        this.op1 = null;
        this.op2 = null;
        this.jump = null;
        this.func = func;
    }
    
    public OperationCode getCode() {
        return code;
    }

    public Variable getTarget() {
        return target;
    }

    public Value getOp1() {
        return op1;
    }

    public Value getOp2() {
        return op2;
    }

    public Block getJump() {
        return jump;
    }

    public Function getFunc() {
        return func;
    }

    @Override
    public String toString() {
        if (func != null) 
            return code.toString() + " " + (target == null ? "" : target.getName()) + ", " + func.getSignature() + ";";
        else if (jump == null)
            return code.toString() + " " + (target == null ? "" : target.getName()) + ", " + op1 + ", " + op2 + ";";
        else
            return code.toString() + " " + jump.getLabel() + ", " + op1 + ";";
    }
    
    
}
