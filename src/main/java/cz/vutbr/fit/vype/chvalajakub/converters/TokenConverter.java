/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.converters;

import cz.vutbr.fit.vype.chvalajakub.VypeParser;
import cz.vutbr.fit.vype.chvalajakub.beans.DataType;
import cz.vutbr.fit.vype.chvalajakub.beans.Variable;
import cz.vutbr.fit.vype.chvalajakub.exceptions.InternalErrors;
import cz.vutbr.fit.vype.chvalajakub.exceptions.SemanticError;
import java.util.List;

/**
 *
 * @author rodney2
 */
public class TokenConverter {
    private static TokenConverter instance;
    
    public static TokenConverter getInstance() {
        if (instance == null) instance = new TokenConverter();
        return instance;
    }
    
    public DataType convertType(VypeParser.TypeContext ctx) {
        if (ctx.dataType() == null)
            return null;
        else
            return convertDataType(ctx.dataType());
    }
    
    public DataType convertDataType(VypeParser.DataTypeContext ctx) {
        switch (ctx.getStart().getType()) {
            case VypeParser.INT_TYPE: return DataType.INT;
            //case VypeParser.SHORT: return DataType.shortType;
            case VypeParser.CHAR_TYPE: return DataType.CHAR;
            case VypeParser.STRING_TYPE: return DataType.STRING;
            default: throw SemanticError.invalidTokenType(ctx.getStart());
        }
    }
    
    public DataType[] convertTypeList(List<? extends VypeParser.TypeContext> typeList) {
        DataType[] ret = new DataType[typeList.size()];
        int i = 0;
        for (VypeParser.TypeContext ctx : typeList) {
           ret[i++] = convertType(ctx);
        }
        return ret;
    }
    
    public DataType[] convertDataTypeList(List<? extends VypeParser.DataTypeContext> typeList) {
        DataType[] ret = new DataType[typeList.size()];
        int i = 0;
        for (VypeParser.DataTypeContext ctx : typeList) {
           ret[i++] = convertDataType(ctx);
        }
        return ret;
    }
    
    public DataType[] convertParamList(List<? extends VypeParser.VarDecl1Context> paramList) {
        if (paramList == null) {
            return new DataType[0];
        }
        else {
            DataType[] ret = new DataType[paramList.size()];
            int i = 0;
            for (VypeParser.VarDecl1Context vdCtx : paramList) {
               ret[i++] = convertDataType(vdCtx.dataType());
            }
            return ret;
        }
    }
    
    public Variable convertVarDecl(VypeParser.VarDecl1Context ctx) {
        Variable var = new Variable();
        var.setName(ctx.ID().getText());
        var.setType(convertDataType(ctx.dataType()));
        return var;
    }
}
