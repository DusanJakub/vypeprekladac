/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans;

/**
 *
 * @author rodney2
 */
public class VarState {
    private final boolean live;
    private final Integer nextUsage;
    private boolean fixed;



    public VarState(boolean live, Integer nextUsage) {
        this.live = live;
        this.nextUsage = nextUsage;
    }

    public boolean isLive() {
        return live;
    }

    public Integer getNextUsage() {
        return nextUsage;
    }
    
    public void fix() {
        fixed = true;
    }
    
    public void unfix() {
        fixed = false;
    }

    public boolean isFixed() {
        return fixed;
    }
    
    

    @Override
    public String toString() {
        return (live ? "L" : "D") + ":" + nextUsage;
    }
    
    
    
}
