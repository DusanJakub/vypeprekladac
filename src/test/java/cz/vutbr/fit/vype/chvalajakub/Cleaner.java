/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */
package cz.vutbr.fit.vype.chvalajakub;

import java.io.File;
import java.io.FileFilter;
import org.junit.Test;

/**
 *
 * @author admin
 */
public class Cleaner extends AbstractTest {

    @Test
    public void cleaning() {
        File testFolder = getFolderWithTests("");
        File[] directoriesWithtest = testFolder.listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });
        
        for(File dir: directoriesWithtest){
            clearGeneratedFilesForTest(dir.getName());
        }
    }
}
