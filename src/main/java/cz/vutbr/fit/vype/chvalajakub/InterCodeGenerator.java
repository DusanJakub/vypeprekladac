/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub;

import cz.vutbr.fit.vype.chvalajakub.beans.Constant;
import cz.vutbr.fit.vype.chvalajakub.beans.DataType;
import cz.vutbr.fit.vype.chvalajakub.beans.Function;
import cz.vutbr.fit.vype.chvalajakub.beans.RootScope;
import cz.vutbr.fit.vype.chvalajakub.beans.Scope;
import cz.vutbr.fit.vype.chvalajakub.beans.Value;
import cz.vutbr.fit.vype.chvalajakub.beans.Variable;
import cz.vutbr.fit.vype.chvalajakub.beans.instructions.Block;
import cz.vutbr.fit.vype.chvalajakub.beans.instructions.Instruction;
import cz.vutbr.fit.vype.chvalajakub.beans.instructions.OperationCode;
import cz.vutbr.fit.vype.chvalajakub.converters.TokenConverter;
import cz.vutbr.fit.vype.chvalajakub.exceptions.InternalErrors;
import cz.vutbr.fit.vype.chvalajakub.exceptions.SemanticError;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author rodney2
 */
public class InterCodeGenerator extends VypeBaseVisitor<Value>{

    private Scope currentScope = new RootScope();
    private Function currentFunction;
    private Block currentBlock;
    private final Map<String,Function> functions = new HashMap<String, Function>();
    
    private final TokenConverter tokenConverter = TokenConverter.getInstance();

    
    /* ********************************************************************** *
     *                    Declarations and definitions                        *
     * ********************************************************************** */
    @Override
    public Value visitVarDecl(VypeParser.VarDeclContext ctx) {
        DataType type = tokenConverter.convertDataType(ctx.dataType());
        for (VypeParser.InitIdContext initIdCtx : ctx.initId()) {
            String name = initIdCtx.ID().getText();
            if (currentScope.contains(name)) {
                throw SemanticError.variableRedefined(name);
            }
            Variable var = new Variable();
            var.setName(name);
            var.setType(type);
            currentScope.put(var);
            
            
            Value init = initIdCtx.exp() != null 
                    ? visit(initIdCtx.exp())
                    : currentScope.registerConstant(Value.getDefault(type), type);
            
            
            if (!implicitCast(var, init)) {
                throw SemanticError.assignTypeMismatch(init.getType(), var.getType());
            }
            
                       
            if (init.isConstant()) {
                var.setDefaultValue(init.asConstant());
            }
            else {
                instr(OperationCode.MOV, var, init, null);
            }
            
        }
        return null;
    }

    @Override
    public Value visitFunctionDecl(VypeParser.FunctionDeclContext ctx) {
        Function func = new Function();
        func.setName(ctx.name.getText());
        if (ctx.types != null)
            func.setParamTypes(tokenConverter.convertDataTypeList(ctx.types));
        func.setReturnType(tokenConverter.convertType(ctx.type()));
        
        if (isBuiltinFunctionName(func.getName())) {
            throw SemanticError.reservedFunctionName(func.getName());
        }
        if (functions.containsKey(func.getSignature())) {
            throw SemanticError.functionAllreadyDefinedOrDeclared(func.getSignature());
        }
        
        functions.put(func.getSignature(), func);
        return null;
    }
    
    @Override
    public Value visitFunctionDef(VypeParser.FunctionDefContext ctx) {
        String name = ctx.name.getText();
        DataType[] paramTypes = tokenConverter.convertParamList(ctx.params);
        DataType returnType = tokenConverter.convertType(ctx.ret);
        
        // TODO: check agains built-ins
        Function func = functions.get(Function.getSignature(name, paramTypes));
        
        
        if (isBuiltinFunctionName(name)) {
            throw SemanticError.reservedFunctionName(name);
        }
        
        if (func != null) {
            if (func.isDefined()) {
                throw SemanticError.functionAllreadyDefined(name);
            }
            if (func.getReturnType() != returnType) {
                throw SemanticError.definedFunctionReturnTypeMismatch(name);
            } 
        }
        else {
            func = new Function();
            func.setName(name);
            func.setParamTypes(paramTypes);
            func.setReturnType(returnType);
                  
            functions.put(func.getSignature(), func);
        }
        
        Scope paramScope = currentScope.getSubScope();
        Variable[] params = new Variable[paramTypes.length];
        int i = 0;
        if (ctx.params != null) {
            for (VypeParser.VarDecl1Context vdCtx : ctx.params) {
                params[i] = tokenConverter.convertVarDecl(vdCtx);
                paramScope.put(params[i]);
                i++;
            }
        }
        func.setParamScope(paramScope);
        currentFunction = func;
        
        Block block = new Block(paramScope.getSubScope(), func.getSignature() + ".begin");
        enterBlock(block);
        instr(OperationCode.INIT_FRAME, block, null);
        
        visitBlock(ctx.block());
        
        instr(OperationCode.RET, null, null, null);
        
        currentScope = paramScope.getParent();
        
        return null;
    }

    /* ********************************************************************** *
     *                              Statements                                *
     * ********************************************************************** */
    @Override
    public Value visitAssignStmt(VypeParser.AssignStmtContext ctx) {
        String targetName = ctx.ID().getText();
        Variable target = currentScope.get(targetName);
        if (target == null)
            throw SemanticError.variableUndefined(targetName);
        
        Value val = visit(ctx.exp());
        
        if (!implicitCast(target, val))
            throw SemanticError.assignTypeMismatch(val.getType(), target.getType());
        
        instr(OperationCode.MOV, target, val, null);
        
        /*switch (target.getType()) {
            case INT: 
            case CHAR:
                instr(OperationCode.MOVI, target, val, null);
                break;
            case STRING:
                instr(OperationCode.MOVS, target, val, null);
                break;
        }*/
        
        
        return null;
    }

    @Override
    public Value visitReturnStmt(VypeParser.ReturnStmtContext ctx) {
        Variable target = null;
        Value val = null;
        if (ctx.exp() != null) {
            if (currentFunction.getReturnType() == null)
                throw SemanticError.returnInVoidRetFnc();
            val = visit(ctx.exp());
            target = currentScope.createCompilerVar(currentFunction.getReturnType());
            if (!implicitCast(target, val))
                throw SemanticError.castMismatchInReturnStmnt(val.getType(), target.getType());
        }
        
        // TODO: what about casting?
        instr(OperationCode.RET, null, val, null);
        return null;
    }

    @Override
    public Value visitFuncStmt(VypeParser.FuncStmtContext ctx) {
        List<Value> params = new ArrayList<Value>(ctx.params.size());
        for (VypeParser.ExpContext paramCtx : ctx.params) {
            params.add(visit(paramCtx));
        }
        
        callFunction(ctx.ID().getText(), params);
        return null;
    }

    @Override
    public Value visitWhileStmt(VypeParser.WhileStmtContext ctx) {
        Block test = new Block(currentScope, currentFunction.getSignature()+".whileTest");
        Block body = new Block(currentScope.getSubScope(), currentFunction.getSignature()+".whileBody");
        Block cont = new Block(currentScope, currentFunction.getSignature()+".whileCont");
        
        instr(OperationCode.STORE, null, null, null);
        
        test.addCaller(currentBlock);
        
        enterBlock(test);
        Value op1 = visit(ctx.exp());
        if (op1.getType() != DataType.INT) {
            throw SemanticError.invalidTypeInCondition("while", op1.getType());
        }
        instr(OperationCode.STORE, null, null, null);
        instr(OperationCode.JEZ, cont, op1);
        body.addCaller(currentBlock);
        cont.addCaller(currentBlock);
        
        enterBlock(body);
        instr(OperationCode.INIT_FRAME, body, null);
        visit(ctx.block());
        instr(OperationCode.STORE, null, null, null);
        instr(OperationCode.JMP, test, null);
        test.addCaller(currentBlock);
        
        enterBlock(cont);
        
        return null;
    }

    @Override
    public Value visitIfElseStmt(VypeParser.IfElseStmtContext ctx) {
        Block positive = new Block(currentScope.getSubScope(), currentFunction.getSignature()+".ifPositive");
        Block negative = new Block(currentScope.getSubScope(), currentFunction.getSignature()+".ifNegative");
        Block cont = new Block(currentScope, currentFunction.getSignature()+".ifCont");
        
        Value op1 = visit(ctx.exp());
        if (op1.getType() != DataType.INT) {
            throw SemanticError.invalidTypeInCondition("if", op1.getType());
        }
        
        positive.addCaller(currentBlock);
        negative.addCaller(currentBlock);
        instr(OperationCode.STORE, null, null, null);
        instr(OperationCode.JEZ, negative, op1);
        
        enterBlock(positive);
        instr(OperationCode.INIT_FRAME, positive, null);
        visit(ctx.ifBlock);
        instr(OperationCode.STORE, null, null, null);
        instr(OperationCode.JMP, cont, null);
        cont.addCaller(currentBlock);
        
        enterBlock(negative);
        instr(OperationCode.INIT_FRAME, negative, null);
        visit(ctx.elseBlock);
        cont.addCaller(currentBlock);
        
        instr(OperationCode.STORE, null, null, null);
        enterBlock(cont);
        
        return null;
    }

    @Override
    public Value visitIfOnlyStmt(VypeParser.IfOnlyStmtContext ctx) {
        Block positive = new Block(currentScope.getSubScope(), currentFunction.getSignature()+".if");
        Block cont = new Block(currentScope, currentFunction.getSignature()+".ifCont");
        
        Value op1 = visit(ctx.exp());
        if (op1.getType() != DataType.INT) {
            throw SemanticError.invalidTypeInCondition("if", op1.getType());
        }
        positive.addCaller(currentBlock);
        cont.addCaller(currentBlock);
        instr(OperationCode.STORE, null, null, null);
        instr(OperationCode.JEZ, cont, op1);
        
        enterBlock(positive);
        instr(OperationCode.INIT_FRAME, positive, null);
        visit(ctx.ifBlock);
        instr(OperationCode.STORE, null, null, null);
        cont.addCaller(currentBlock);
        
        enterBlock(cont);
        
        return null;
    }

    @Override
    public Value visitBlock(VypeParser.BlockContext ctx) {
        super.visitBlock(ctx);
        currentFunction.enlargeLocalFrameSize(currentScope.totalSize());
        return null;
    }
    
    

    
    
    
    
    

    
    
    
    
    
    /* ********************************************************************** *
     *                              Expressions                               *
     * ********************************************************************** */
    
    @Override
    public Value visitNotExpr(VypeParser.NotExprContext ctx) {
        Value op1 = visit(ctx.exp());
        
        DataType targetType;
        OperationCode opcode;
        
        if (op1.getType() == DataType.INT)
            targetType = DataType.INT;
        else
            throw SemanticError.invalidOperandTypesError(ctx.op.getText(), op1.getType(), null);
        
        Variable target = currentScope.createCompilerVar(targetType);
        
        switch (ctx.op.getType()) {
            case VypeLexer.NOT: opcode = OperationCode.NOT; break;
            default: throw InternalErrors.unknownOpCode(ctx.op);
        }
        
        instr(opcode, target, op1, null);
        return target;
    }

    @Override
    public Value visitUnaryExpr(VypeParser.UnaryExprContext ctx) {
        Value op1 = visit(ctx.exp());
        
        DataType targetType;
        OperationCode opcode;
        
        if (op1.getType() == DataType.INT)
            targetType = DataType.INT;
        else
            throw SemanticError.invalidOperandTypesError(ctx.op.getText(), op1.getType(), null);
        
        
        switch (ctx.op.getType()) {
            case VypeLexer.PLUS: 
                return op1;
            case VypeLexer.MINUS: 
                if (op1.isConstant()) {
                    String value = "-" + op1.asConstant().getValue();
                    return currentScope.registerConstant(value.replace("--", ""), op1.getType());
                }
                else {
                    Variable target = currentScope.createCompilerVar(targetType);
                    instr(OperationCode.NEG, target, op1, null);
                    return target;
                }
            default: 
                throw InternalErrors.unknownOpCode(ctx.op);
        }
        
        
    }
    
    
    
    

    @Override
    public Value visitMultExpr(VypeParser.MultExprContext ctx) {
        Value op1 = visit(ctx.op1);
        Value op2 = visit(ctx.op2);
        
        DataType targetType;
        OperationCode opcode;
        
        if (op1.getType() == DataType.INT && op2.getType() == DataType.INT)
            targetType = DataType.INT;
        else
            throw SemanticError.invalidOperandTypesError(ctx.op.getText(), op1.getType(), op2.getType());
        
        Variable target = currentScope.createCompilerVar(targetType);
        
        switch (ctx.op.getType()) {
            case VypeLexer.TIMES: opcode = OperationCode.MUL; break;
            case VypeLexer.DIV: opcode = OperationCode.DIV; break;
            case VypeLexer.REM: opcode = OperationCode.REM; break;
            default: throw InternalErrors.unknownOpCode(ctx.op);
        }
        
        instr(opcode, target, op1, op2);
        return target;
    }
    
    @Override
    public Value visitAddExpr(VypeParser.AddExprContext ctx) {
        Value op1 = visit(ctx.op1);
        Value op2 = visit(ctx.op2);
        
        DataType targetType;
        OperationCode opcode;
        
        if (op1.getType() == DataType.INT && op2.getType() == DataType.INT)
            targetType = DataType.INT;
        else
            throw SemanticError.invalidOperandTypesError(ctx.op.getText(), op1.getType(), op2.getType());
        
        Variable target = currentScope.createCompilerVar(targetType);
        
        switch (ctx.op.getType()) {
            case VypeLexer.PLUS: opcode = OperationCode.ADD; break;
            case VypeLexer.MINUS: opcode = OperationCode.SUB; break;
            default: throw InternalErrors.unknownOpCode(ctx.op);
        }
        
        instr(opcode, target, op1, op2);
        return target;
    }

    @Override
    public Value visitCompExpr(VypeParser.CompExprContext ctx) {
        Value op1 = visit(ctx.op1);
        Value op2 = visit(ctx.op2);
        
        DataType targetType;
        OperationCode opcode;
        
        if (op1.getType() == op2.getType())
            targetType = DataType.INT;
        else
            throw SemanticError.invalidOperandTypesError(ctx.op.getText(), op1.getType(), op2.getType());
        
        Variable target = currentScope.createCompilerVar(targetType);
        
        if (op1.getType() == DataType.STRING) {
            switch (ctx.op.getType()) {
                case VypeLexer.LT: opcode = OperationCode.LTS; break;
                case VypeLexer.GT: opcode = OperationCode.GTS; break;
                case VypeLexer.LTE: opcode = OperationCode.LTES; break;
                case VypeLexer.GTE: opcode = OperationCode.GTES; break;
                default: throw InternalErrors.unknownOpCode(ctx.op);
            }
        }
        else {
            switch (ctx.op.getType()) {
                case VypeLexer.LT: opcode = OperationCode.LTI; break;
                case VypeLexer.GT: opcode = OperationCode.GTI; break;
                case VypeLexer.LTE: opcode = OperationCode.LTEI; break;
                case VypeLexer.GTE: opcode = OperationCode.GTEI; break;
                default: throw InternalErrors.unknownOpCode(ctx.op);
            }
        }
        
        instr(opcode, target, op1, op2);
        return target;
    }

    @Override
    public Value visitEqualsExpr(VypeParser.EqualsExprContext ctx) {
        Value op1 = visit(ctx.op1);
        Value op2 = visit(ctx.op2);
        
        DataType targetType;
        OperationCode opcode;
        
        if (op1.getType() == op2.getType())
            targetType = DataType.INT;
        else
            throw SemanticError.invalidOperandTypesError(ctx.op.getText(), op1.getType(), op2.getType());
        
        Variable target = currentScope.createCompilerVar(targetType);
        
        if (op1.getType() == DataType.STRING) {
            switch (ctx.op.getType()) {
                case VypeLexer.EQ: opcode = OperationCode.EQS; break;
                case VypeLexer.NEQ: opcode = OperationCode.NEQS; break;
                default: throw InternalErrors.unknownOpCode(ctx.op);
            }
        }
        else {
            switch (ctx.op.getType()) {
                case VypeLexer.EQ: opcode = OperationCode.EQI; break;
                case VypeLexer.NEQ: opcode = OperationCode.NEQI; break;
                default: throw InternalErrors.unknownOpCode(ctx.op);
            }
        }
        
        instr(opcode, target, op1, op2);
        return target;
    }
    
    @Override
    public Value visitAndExpr(VypeParser.AndExprContext ctx) {
        
        Block blockOp2 = new Block(currentScope, currentFunction.getSignature()+".andSecond");
        Block cont = new Block(currentScope, currentFunction.getSignature()+".andCont");
        
        
        Variable target = currentScope.createCompilerVar(DataType.INT);
        
        instr(OperationCode.MOV, target, currentScope.registerConstant("0", DataType.INT), null);
        Value op1 = visit(ctx.op1);
        instr(OperationCode.STORE, null, null, null);
        instr(OperationCode.JEZ, cont, op1);
        blockOp2.addCaller(currentBlock);
        cont.addCaller(currentBlock);
        
        enterBlock(blockOp2);
        Value op2 = visit(ctx.op2);
        instr(OperationCode.MOVB, target, op2, null);
        instr(OperationCode.STORE, null, null, null);
        cont.addCaller(currentBlock);
        
        enterBlock(cont);
        
        if (op1.getType()!= DataType.INT || op2.getType() != DataType.INT)
            throw SemanticError.invalidOperandTypesError(ctx.op.getText(), op1.getType(), op2.getType());
        
        return target;
    }

    @Override
    public Value visitOrExpr(VypeParser.OrExprContext ctx) {
        
        Block blockOp2 = new Block(currentScope, currentFunction.getSignature()+".orSecond");
        Block cont = new Block(currentScope, currentFunction.getSignature()+".orCont");
        
        
        Variable target = currentScope.createCompilerVar(DataType.INT);
        
        instr(OperationCode.MOV, target, currentScope.registerConstant("1", DataType.INT), null);
        Value op1 = visit(ctx.op1);
        instr(OperationCode.STORE, null, null, null);
        instr(OperationCode.JNZ, cont, op1);
        blockOp2.addCaller(currentBlock);
        cont.addCaller(currentBlock);
        
        enterBlock(blockOp2);
        Value op2 = visit(ctx.op2);
        instr(OperationCode.MOVB, target, op2, null);
        instr(OperationCode.STORE, null, null, null);
        cont.addCaller(currentBlock);
        
        enterBlock(cont);
        
        if (op1.getType()!= DataType.INT || op2.getType() != DataType.INT)
            throw SemanticError.invalidOperandTypesError(ctx.op.getText(), op1.getType(), op2.getType());
        
        return target;
    }

    @Override
    public Value visitParensExpr(VypeParser.ParensExprContext ctx) {
        return visit(ctx.exp());
    }

    @Override
    public Value visitCastExpr(VypeParser.CastExprContext ctx) {
        Value op1 = visit(ctx.exp());
        DataType targetType = tokenConverter.convertDataType(ctx.dataType());
        
        if (op1.getType() == targetType) {
            return op1;
        }
        
        OperationCode opcode;
        Variable target = currentScope.createCompilerVar(targetType);
        if (op1.getType() == DataType.CHAR && targetType == DataType.STRING) {
            opcode = OperationCode.C2S;
        }
        else if (op1.getType() == DataType.CHAR && targetType == DataType.INT) {
            opcode = OperationCode.C2I;
        }
        else if (op1.getType() == DataType.INT && targetType == DataType.CHAR) {
            opcode = OperationCode.I2C;
        }
        else {
            throw SemanticError.castMismatch(op1.getType(), targetType);
        }
        
        instr(opcode, target, op1, null);
        return target;
    }

    @Override
    public Value visitIdExpr(VypeParser.IdExprContext ctx) {
        Variable var = currentScope.get(ctx.ID().getText());
        if (var == null)
            throw SemanticError.variableUndeclared(ctx.ID().getText());
        return var;
    }

    @Override
    public Value visitConstant(VypeParser.ConstantContext ctx) {
        DataType type;
        if (ctx.CHAR_CONSTANT() != null) {
            type = DataType.CHAR;
        }
        else if (ctx.INT_CONSTANT() != null) {
            type = DataType.INT;
        }
        else if (ctx.STRING_CONSTANT() != null) {
            type = DataType.STRING;
        } 
        else {
            throw InternalErrors.getInternalError("Unknown type of the constant: " + ctx.getText());
        }
        
        return currentScope.registerConstant(ctx.getText(), type);
    }
//    
//    
//    
//    @Override
//    public Value visitIntExpr(VypeParser.IntExprContext ctx) {
//        return currentScope.registerConstant(ctx.getText(), DataType.INT);
//    }
//
//    @Override
//    public Value visitCharExpr(VypeParser.CharExprContext ctx) {
//        return currentScope.registerConstant(ctx.getText(), DataType.CHAR);
//    }
//
//    @Override
//    public Value visitStringExpr(VypeParser.StringExprContext ctx) {
//        return currentScope.registerConstant(ctx.getText(), DataType.STRING);
//    }

    @Override
    public Value visitFuncExpr(VypeParser.FuncExprContext ctx) {
        List<Value> params = new ArrayList<Value>(ctx.params.size());
        
        for (VypeParser.ExpContext paramCtx : ctx.params) {
            params.add(visit(paramCtx));
        }
        
        Variable target = callFunction(ctx.ID().getText(), params);
        if (target == null) {
            throw SemanticError.voidFunctionInExpression(ctx.ID().getText());
        }
        return target;
    }
    
    
    /**
     * Checks whether the value can be assigned to the variable
     * If so, implicit casting operation can be added to the instruction list
     * @param to
     * @param from
     * @return 
     */
    public boolean implicitCast(Variable to, Value from) {
        return to.getType() == from.getType();
    }
    
    protected Variable callFunction(String name, List<Value> params) {
        if (params == null) params = Collections.emptyList();
        
        if ("print".equals(name) && params.size() > 0) {
            for (Value par : params) {
                switch (par.getType()) {
                    case INT:
                        instr(OperationCode.PRINTI, null, par, null);
                        break;
                    case CHAR: 
                        instr(OperationCode.PRINTC, null, par, null);
                        break;
                    case STRING:
                        instr(OperationCode.PRINTS, null, par, null);
                        break;
                }
            }
            return null;
        }
        
        Variable target = null;
        DataType[] paramTypes = new DataType[params.size()];
        int i=0;
        for (Value par : params) {
            paramTypes[i++] = par.getType();
        }
        
        String sig = Function.getSignature(name, paramTypes);
        Function func;
        
        if ("read_char.".equals(sig)) {
            target = currentScope.createCompilerVar(DataType.CHAR);
            instr(OperationCode.READ_CHAR, target, null, null);
        }
        else if ("read_int.".equals(sig)) {
            target = currentScope.createCompilerVar(DataType.INT);
            instr(OperationCode.READ_INT, target, null, null);
            return target;
        }
        else if ("read_string.".equals(sig)) {
            target = currentScope.createCompilerVar(DataType.STRING);
            instr(OperationCode.READ_STR, target, null, null);
        }
        else if ("get_at.Si".equals(sig)) {
            target = currentScope.createCompilerVar(DataType.CHAR);
            instr(OperationCode.MOVCS, target, params.get(0), params.get(1));
        }
        else if ("set_at.Sic".equals(sig)) {
            target = currentScope.createCompilerVar(DataType.STRING);
            Variable sub = currentScope.createCompilerVar(DataType.STRING);
            
            instr(OperationCode.MOVS, target, params.get(0), null);
            instr(OperationCode.ADD, sub, target, params.get(1));
            instr(OperationCode.MOVSC, null, sub, params.get(2));
        }
        else if ("strcat.SS".equals(sig)) {
            target = currentScope.createCompilerVar(DataType.STRING);
            instr(OperationCode.STRCAT, target, params.get(0), params.get(1));
        }
        else if ((func = functions.get(sig)) != null) {
            // argument to PAR can only be variable, not constant
            ListIterator<Value> it = params.listIterator();
            while (it.hasNext()) {
                Value v = it.next();
                if (!v.isVariable()) {
                    Variable tmp = currentScope.createCompilerVar(v.getType());
                    instr(OperationCode.MOV, tmp, v, null);
                    it.set(tmp);
                }
            }
            for (Value param : params) {
                instr(OperationCode.PAR, null, param, null);
            }
            target = func.getReturnType() == null ? null : currentScope.createCompilerVar(func.getReturnType());
            instr(OperationCode.CAL, target, func);
            currentFunction.enlargeCallArgsFrameSize(params.size());
        }
        else {
            throw  SemanticError.functionUndeclared(sig);
        }
        
        return target;
    }
    

    public Map<String, Function> getFunctions() {
        return functions;
    }
    
    public Collection<Constant> getConstants() {
        return currentScope.getConstants();
    }
    
    
    protected void enterBlock(Block block) {
        currentFunction.addBlock(block);
        currentBlock = block;
        currentScope = block.getScope();
    }
    
    protected void instr(OperationCode code, Block jump, Value op1) {
        currentBlock.addInstruction(new Instruction(code, jump, op1));
    }
    
    protected void instr(OperationCode code, Variable target, Value op1, Value op2) {
        currentBlock.addInstruction(new Instruction(code, target, op1, op2));
    }
    
    protected void instr(OperationCode code, Variable target, Function func) {
        currentBlock.addInstruction(new Instruction(code, target, func));
    }
    
    /**
     * Can be called only from outside of the generator, 
     * after the whole code has been generated.
     * Otherwise current scope might not be the root scope.
     * @return 
     */
    public RootScope getRootScope() {
        return (RootScope) currentScope;
    }
    
    /**
     * function checks whether all declared functions were defined.
     * 
     * @param optim - if Optimizer is not null, then defined functions are analyzed with given Optimizer
     */
    public void checkDeclaredFunctionsAreDefined(Analyzer optim){
        for (Function f : getFunctions().values()) {
            if (!f.isDefined()) {
                throw SemanticError.functionUndefined(f.getSignature());
            } else if (optim != null) {
                optim.analyzeFunction(f);
            }
        }
    }
    
    public boolean isBuiltinFunctionName(String name) {
        return "print".equalsIgnoreCase(name)
                || "read_char".equalsIgnoreCase(name)
                || "read_int".equalsIgnoreCase(name)
                || "read_string".equalsIgnoreCase(name)
                || "get_at".equalsIgnoreCase(name)
                || "set_at".equalsIgnoreCase(name)
                || "strcat".equalsIgnoreCase(name);
    }
         
}
