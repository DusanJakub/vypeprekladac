/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */
package cz.vutbr.fit.vype.chvalajakub;

import cz.vutbr.fit.vype.chvalajakub.beans.AppReturnCode;
import cz.vutbr.fit.vype.chvalajakub.exceptions.TranslationException;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public abstract class AbstractTest {

    public AbstractTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    protected void processTest(String test) {
        System.out.println("----------------------------------------------- " + test);
        clearGeneratedFilesForTest(test);
        File asmFolder = getAsmFolder();
        File testsFolder = getFolderWithTests(test);

        String source, outputAsm, outputAsmObj, compiledAsm, xmlOutput;
        File stdOut, stdErr;
        Integer expectedResult, result;

        source = new File(testsFolder, test + ".vype").getAbsolutePath();
        outputAsm = new File(testsFolder, test + ".asm").getAbsolutePath();

        String[] args = new String[]{source, outputAsm};
        AppReturnCode compileResult = AppReturnCode.NO_ERR;
        expectedResult = Integer.valueOf(test.substring(0, 1));

        try {
            App.mainFunction(args);
        } catch (TranslationException e) {
            compileResult = e.getRetCode();
            
            System.out.println(e.getMessage());
            if (compileResult.getReturnCode() != expectedResult) {
                fail(String.format("Test [%s] failed. Returned code: %s, expected code: %s. | msg: %s", test, compileResult.getReturnCode(), expectedResult, e.getMessage()));
            }
        }

        if (compileResult.getReturnCode() != expectedResult) {
            fail(String.format("Test [%s] failed. Returned code: %s, expected code: %s.", test, compileResult.getReturnCode(), expectedResult));
        } else if (expectedResult == AppReturnCode.NO_ERR.getReturnCode()) {
            try {
                outputAsmObj = new File(testsFolder, test + ".obj").getAbsolutePath();
                compiledAsm = new File(testsFolder, test + ".xexe").getAbsolutePath();
                xmlOutput = new File(testsFolder, test + ".xml").getAbsolutePath();
                stdOut = new File(testsFolder, test + ".stdout");
                stdErr = new File(testsFolder, test + ".err");

                ProcessBuilder asmCompileB = new ProcessBuilder(getProcessCompileArgs(asmFolder, outputAsm, outputAsmObj));
                Process compilation = asmCompileB.start();
                compilation.waitFor();

                ProcessBuilder asmLinkerB = new ProcessBuilder(getProcessLinkerArgs(asmFolder, outputAsmObj, compiledAsm));
                Process linking = asmLinkerB.start();
                linking.waitFor();

                ProcessBuilder builder = new ProcessBuilder(getProcessSimulationArgs(asmFolder, compiledAsm, xmlOutput));

                builder.redirectOutput(stdOut);
                builder.redirectError(stdErr);

                File input = new File(testsFolder, test + ".in");
                if (input.exists()) {
                    builder.redirectInput(input);
                }

                Process simulation = builder.start();
                simulation.waitFor();

                File expectedOutput = new File(testsFolder, test + ".out");
                if (expectedOutput.exists()) {
                    if (!stdOut.exists() || !fileContentStartsWithFileContent(stdOut, expectedOutput)) {
                        fail(String.format("Test [%s] failed. Expected output does not match output.", test));
                    }
                } else {
                    fail(String.format("Test [%s] failed. You must provide file with expected output.", test));
                }
            } catch (IOException ex) {
                Logger.getLogger(AbstractTest.class.getName()).log(Level.SEVERE, null, ex);
                fail("failed" + ex.getMessage());
            } catch (InterruptedException ex) {
                Logger.getLogger(AbstractTest.class.getName()).log(Level.SEVERE, null, ex);
                fail("failed" + ex.getMessage());
            }
        }
    }

    private List<String> getProcessCompileArgs(File asmFolder, String outputAsm, String outputAsmObj) {
        ArrayList<String> result = new ArrayList<String>();
        addWineIfOnLinux(result);
        result.add(String.format("%s" + File.separator + "assembler2.exe", asmFolder.getAbsolutePath()));
        result.add("-i");
        result.add(outputAsm);
        result.add("-o");
        result.add(outputAsmObj);
        return result;
    }

    private List<String> getProcessLinkerArgs(File asmFolder, String outputAsmObj, String compiledAsm) {
        ArrayList<String> result = new ArrayList<String>();
        addWineIfOnLinux(result);
        result.add(String.format("%s" + File.separator + "linker.exe", asmFolder.getAbsolutePath()));
        result.add(outputAsmObj);
        result.add("-o");
        result.add(compiledAsm);
        return result;
    }

    private List<String> getProcessSimulationArgs(File asmFolder, String compiledAsm, String xmlOutput) {
        ArrayList<String> result = new ArrayList<String>();
        addWineIfOnLinux(result);
        result.add(String.format("%s" + File.separator + "intersim2.exe", asmFolder.getAbsolutePath()));
        result.add("-i");
        result.add(compiledAsm);
        result.add("-x");
        result.add(xmlOutput);
        result.add("-n");
        result.add("mips");
        return result;
    }

    private void addWineIfOnLinux(ArrayList<String> args) {
        String os = System.getProperty("os.name");
        if (args != null && os.contains("Linux")) {
            args.add("wine");
        }
    }

    protected File getProjectFolder() {
        return new File(Paths.get("").toAbsolutePath().toString());
    }

    protected File getAsmFolder() {
        return new File(getProjectFolder(), "/asm");
    }

    protected File getFolderWithTests(String testName) {

        return new File(getProjectFolder(), "/test/" + testName);
    }

    protected void clearGeneratedFilesForTest(String testName) {
        File testFolder = getFolderWithTests(testName);
        File[] filesToDelete = testFolder.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                String filename = pathname.getName();
                return filename.endsWith(".asm")
                        || filename.endsWith(".obj")
                        || filename.endsWith(".xexe")
                        || filename.endsWith(".stdout")
                        || filename.endsWith(".xml")
                        || filename.endsWith(".err")
                        || filename.endsWith(".diff");
            }
        });

        if (filesToDelete != null) {
            for (File f : filesToDelete) {
                f.delete();
            }
        }
    }

    private boolean fileContentStartsWithFileContent(File output, File expectedOutput) throws FileNotFoundException, IOException {
        String f1c, f2c;
        f1c = readFile(output, Charset.forName("UTF-8"));
        f2c = readFile(expectedOutput, Charset.forName("UTF-8"));

        f1c = f1c.replaceAll("\\r?\\n", "\n");
        f2c = f2c.replaceAll("\\r?\\n", "\n");

        return f1c.startsWith(f2c);

//        FileReader expeReader = new FileReader(expectedOutput);
//        FileReader outReader = new FileReader(output);
//
//        int exp;
//        while ((exp = expeReader.read()) != -1) {
//            if (exp != outReader.read()) {
//                return false;
//            }
//        }
//        return true;
    }

    static String readFile(File file, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(file.toPath());
        return encoding.decode(ByteBuffer.wrap(encoded)).toString();
    }

}
