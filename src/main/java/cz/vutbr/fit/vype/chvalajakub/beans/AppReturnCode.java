/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans;

/**
 *
 * @author jan.chvala
 */
public enum AppReturnCode {

    NO_ERR(0), LEX_ERR(1), SYN_ERR(2), SEM_ERR(3), GEN_ERR(4), INTERN_ERR(5);
    
    int returnCode;

    AppReturnCode(int code) {
        this.returnCode = code;
    }

    public int getReturnCode() {
        return returnCode;
    }
}
