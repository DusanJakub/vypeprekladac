/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 *
 * @author rodney2
 */
public class Scope {
    private final Scope parent;
    
    private final Map<String,Variable> vars = new LinkedHashMap<String,Variable>();
    private final Map<DataType,Stack<Variable>> freeVars = new EnumMap<DataType, Stack<Variable>>(DataType.class);

    protected Scope(Scope parent) {
        this.parent = parent;
    }
    
    public Scope getSubScope() {
        Scope sub = new Scope(this);
        return sub;
    }

    public Scope getParent() {
        return parent;
    }
    
    
    private static int compilerVarsCount = 0;
    public Variable createCompilerVar(DataType type) {
        Stack<Variable> stack = freeVars.get(type);
        if (stack != null && !stack.isEmpty()) {
            return stack.pop();
        }
        else {
            String name = "" + Variable.GENERATED_CHAR + type.getInitial() + compilerVarsCount + Variable.GENERATED_CHAR;
            compilerVarsCount++;
            Variable var = new Variable();
            var.setName(name);
            var.setType(type);
            put(var);
            return var;
        }
    }
    
    public void freeCompilerVar(Value val) {
        if (val == null && !(val instanceof Variable)) {
            return;
        }
        
        Variable var = (Variable) val;
        Stack<Variable> stack = freeVars.get(var.getType());
        if (stack == null) {
            stack = new Stack<Variable>();
            freeVars.put(var.getType(), stack);
        }
        stack.push(var);
    }
    
    /**
     * Get the variable from this and the parent scopes by name
     * @param name Variable, null ifnot found
     * @return 
     */
    public Variable get(String name) {
        Variable var = vars.get(name);
        if (var == null && parent != null)
            var = parent.get(name);
        return var;
    }
    
    /**
     * Check is this immediate scope contains the variable. 
     * Parent scopes are not searched.
     * @param name
     * @return 
     */
    public boolean contains(String name) {
        return vars.containsKey(name);
    }

    public Variable put(Variable value) {
        if (value == null) throw new NullPointerException("value");
        value.setOffset(size());
        return vars.put(value.getName(), value);
    }
    
    public List<Variable> getAll() {
        List<Variable> list = new ArrayList<Variable>(totalSize());
        getAll(list);
        return list;
    }
    
    protected void getAll(List<Variable> list) {
        if (parent != null) parent.getAll(list);
        list.addAll(vars.values());
    }
    
    public Collection<Variable> getLocal() {
        return vars.values();
    }
    
    public Constant registerConstant(String value, DataType type) {
        return parent.registerConstant(value, type);
    }
    
    public int size() {
        return vars.size();
    }

    public int totalSize() {
        if (parent == null) {
            return size();
        }
        else {
            return parent.totalSize() + size();
        }
    }
    
    
    public Collection<Constant> getConstants() {
        return parent.getConstants();
    }

    public int getOffset() {
        if (parent == null)
            return 0;
        else
            return parent.getOffset() + parent.size();
    }
    
    

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int offset = getOffset();
        for (Variable var : vars.values()) {
            sb.append("  ").append(var.getType().name()).append(" ").append(var.getName()).append(" [BP+").append(offset + var.getOffset()).append("]\n");
        }
            
        return sb.toString();
    }
}
