/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans.instructions;

/**
 *
 * @author rodney2
 */
public enum OperationCode {
    NEG, MOV, MOVB, MOVS, MOVCS, MOVSC,
    
    ADD, SUB, MUL, DIV, REM,
    CAL, RET, PAR,
    JMP, JEZ, JNZ,
    GTI, LTI, GTEI, LTEI, EQI, NEQI,
    GTS, LTS, GTES, LTES, EQS, NEQS,
    NOT, AND, OR,
    
    C2S, C2I, I2C,
    
    PRINTI, PRINTC, PRINTS, READ_CHAR, READ_INT, READ_STR, STRCAT,
    
    INIT_FRAME, STORE
}
