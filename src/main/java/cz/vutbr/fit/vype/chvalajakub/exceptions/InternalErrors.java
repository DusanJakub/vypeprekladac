/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.exceptions;

import cz.vutbr.fit.vype.chvalajakub.beans.AppReturnCode;
import cz.vutbr.fit.vype.chvalajakub.beans.DataType;
import org.antlr.v4.runtime.Token;

/**
 *
 * @author rodney2
 */
public class InternalErrors extends TranslationException {
    public static final long serialVersionUID = 1;

    public static InternalErrors unableOpenInputFile(String inputFilename) {
        return getInternalError("Unable to open input file [" + inputFilename + "]. ");
    }
    
    public static InternalErrors unableOpenOutputFile(String outputFilename) {
        return getInternalError("Unable to open output file [" + outputFilename + "]. ");
    }
    
    public static InternalErrors unableToWriteToOutputFile(String outputFilename) {
        return getInternalError("Unable to write to output file [" + outputFilename + "]. ");
    }

    public InternalErrors(String message) {
        super(AppReturnCode.INTERN_ERR, message, null);
    }

    public InternalErrors(String message, Throwable cause) {
        super(AppReturnCode.INTERN_ERR, message, cause);
    }
    
    public static InternalErrors unknownOpCode(Token t){
        return getInternalError("Unknown operation code for operation: " + t.getText());
    }
    
    public static InternalErrors printHelp(){
        return getInternalError("Usage: <program> <input file>");
    }
    
    public static InternalErrors getInternalError(String completeMsg){
        return new InternalErrors(completeMsg);
    }
}
