/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */
package cz.vutbr.fit.vype.chvalajakub;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collection;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author admin
 */
@RunWith(Parameterized.class)
public class AllTests extends AbstractTest {
    
    
    @Parameterized.Parameters(name = "{index}| {0}")
    public static Collection<Object[]> getFiles() {
    	Collection<Object[]> params = new ArrayList<Object[]>();
        
        File testFolder = new AllTests("").getFolderWithTests("");
        File[] directoriesWithtest = testFolder.listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });
        
        
    	for (File f : directoriesWithtest) {
    		Object[] arr = new Object[] { f.getName() };
    		params.add(arr);
    	}
    	return params;
    }
    
    private final String testName;

    public AllTests(String testName) {
        this.testName = testName;
    }

    @Test
    public void test() {
        if(testName != null){
            processTest(testName);
        } else {
            fail("test name can not be empty");
        }
    }
}
