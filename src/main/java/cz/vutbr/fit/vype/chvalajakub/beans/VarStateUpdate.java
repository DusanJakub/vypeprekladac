/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author rodney2
 */
public class VarStateUpdate {
    private LinkedHashMap<Variable,VarState> updates = new LinkedHashMap<Variable, VarState>();

    public void put(Variable var, VarState state) {
        updates.put(var, state);
    }
    
    public VarState get(Variable var) {
        return updates.get(var);
    }
    
    
    
    /*public void setTarget(Variable target, VarState targetState) {
        this.targetState = targetState;
        this.target = target;
    }

    public void setOp1(Variable op1, VarState op1State) {
        this.op1State = op1State;
        this.op1 = op1;
    }

    public void setOp2(Variable op2, VarState op2State) {
        this.op2State = op2State;
        this.op2 = op2;
    }*/

    
    
    /*public VarState getTargetState() {
        return targetState;
    }

    public VarState getOp1State() {
        return op1State;
    }

    public VarState getOp2State() {
        return op2State;
    }

    public Variable getTarget() {
        return target;
    }

    public Variable getOp1() {
        return op1;
    }

    public Variable getOp2() {
        return op2;
    }*/

    public Set<Map.Entry<Variable, VarState>> entrySet() {
        return updates.entrySet();
    }
    
    

    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Entry<Variable,VarState> entry : entrySet()) {
            sb.append("\t").append(entry.getKey().getName()).append("=").append(entry.getValue());
        }
        return sb.toString();
    }
    
    
    
    
}
