/*
 * 
 * Compiler of VYPe13 language to MIPS assembly code
 * -------------------------------------------------
 * 
 * Project for VYPe
 * 2013
 * 
 * Authors: Dusan Jakub, xjakub19 and Jan Chvala, xchval01
 * 
 */

package cz.vutbr.fit.vype.chvalajakub.beans;

/**
 *
 * @author rodney2
 */
public abstract class Value {
    protected String name;
    protected DataType type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataType getType() {
        return type;
    }

    public void setType(DataType type) {
        this.type = type;
    }
    
    public boolean isVariable() {
        return (this instanceof Variable);
    }
    
    public Variable asVariable() {
        return (Variable) this;
    }
    
    public boolean isConstant() {
        return (this instanceof Constant);
    }
    
    public Constant asConstant() {
        return (Constant) this;
    }
    
    public static String getDefault(DataType type) {
        switch (type) {
            case STRING: return "\"\"";
            case INT: return "0";
            case SHORT: return "0";
            case CHAR: return "'\\0'";
            default: return "0";
        }
    }
}
